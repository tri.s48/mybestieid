<?php

namespace App\Console\Commands;

use App\Model\Content;
use App\Model\User;
use App\Model\Video;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticLink;
use Illuminate\Console\Command;
use Youtube;
use App\Http\Controllers\Backend\YoutubeController;
use App\Http\Controllers\Backend\GoogleSheetController;


class YoutubeGetAPIMonthly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'YoutubeGetAPIMonthly';
    protected $description = 'Youtube API Monthly';
    protected $youtubeAnalitic;
    protected $googleSheet;

    public function __construct(YoutubeController $youtubeAnalitic, GoogleSheetController $googleSheet)
    {
        parent::__construct();
        $this->youtubeAnalitic = $youtubeAnalitic;
        $this->googleSheet = $googleSheet;
    }    



    public function handle()
    {
        //$singleCreators = User::get();
        $values = $this->googleSheet->create();
        var_dump($values);

        $insertt = YoutubeAnaliticLink::insert([   
                    'link'       => "https://docs.google.com/spreadsheets/d/".$values."/edit", 
                    'fileid'       => $values,
                    'created_at'    => date("Y-m-d H:i:s"),
                    'updated_at'    => date("Y-m-d H:i:s")
                ]);
    }
}
