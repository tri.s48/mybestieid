<?php
#    Output easy-to-read function helpers
#    by Julio Notodiprodyo

use App\Model\Menu;
use App\Model\Page;
use App\Model\SiteConfig;

function menu()
{
    return Menu::select('title', 'url', 'icon')->where('is_active', '=', '1')->get();
}

function page()
{
    return Page::select('title', 'slug', 'attr_3')->where(['is_active' => '1'])->get();
}

function pageTitle($slug)
{
    return Page::select('title')->where(['slug' => $slug])->first();
}

function menuTitle($slug)
{
    $data = Menu::select('title')->where(['url' => $slug])->first();

    !empty($data) ? ($label = $data->title) : ($label = 'Home');
    
    return $label;
}

function webConfig()
{
    $configs = SiteConfig::get();

    $data = [];
    if (sizeof($configs) > 0) {
        foreach ($configs as $key => $value) {
            $data[$value->key] = $value->value;
        }
    }

    return $data;
}