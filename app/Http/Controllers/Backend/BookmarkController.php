<?php

namespace App\Http\Controllers\Backend;

use Flash;
use App\Model\Posts;
use App\Repositories\UserRepository;
use App\Http\Controllers\Backend\Controller;

class BookmarkController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $post;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Posts $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookmarks = auth()->user()->bookmarkPost()->paginate(5);
        
        return view('backend.bookmark.index', ['bookmarks' => $bookmarks]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        auth()->user()->bookmarkPost()->detach($id);

        Flash::success('Bookmark deleted successfully.');

        return redirect(route('creator.bookmark.index'));
    }
}
