<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\UserRepository;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Storage;
use Log;
use App\Model\User;
use App\Model\UserGroup;
use App\Model\UserToken;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticLink;
use App\Model\YoutubePartner;
use Excel;
use Carbon;
use Auth;
use DB;
use PDF;
use Session;
use Youtube;
use Socialite;
use App\Http\Controllers\Auth\LoginController;
use  alchemyguy\YoutubeLaravelApi\ChannelService;


class YoutubeController extends Controller
{

    protected $users;
    protected $authLogin;


    public function __construct(UserRepository $users, LoginController $authLogin)
    {
        $this->users = $users;
        $this->authLogin =  $authLogin;
    }

    public function index()
    {
        $typereporting[''] = 'Please Select';
        $typereporting[0] = 'Weekly';
        $typereporting[1] = 'Monthly';
        $typereporting[2] = 'Yearly';;

        // $channel = Youtube::getChannelByCustomUrl('AsiaMediaProductions');
        // dd($channel);

        return view('youtube.index', [
            'typereporting' => $typereporting
        ]);
    }
    public function analyticdetail(Request $request)
    {
        //return $request;
        // if($request->typereporting == 0 || $request->typereporting == 1){
        //     $date = $request->year.'-'.$request->month;
        // }else{
        //     $date = $request->year;
        // }
        $date = $request->year.'-'.$request->month;
        //return $date;
        // if($request->typereporting == 0 || $request->typereporting == 1){
        //     $chanalytics = DB::select("SELECT name_channel,`subscribers` FROM (SELECT name_channel, `user_id`, Concat(Group_concat(`subscribers`), ',') AS `subscribers` FROM youtube_analitic WHERE Date_format(created_at, '%Y-%m') = '".$date."' GROUP BY name_channel, `user_id`) aggr");
        // }else{
        //     $chanalytics = DB::select("SELECT name_channel,`subscribers` FROM (SELECT name_channel, `user_id`, Concat(Group_concat(`subscribers`), ',') AS `subscribers` FROM youtube_analitic WHERE Date_format(created_at, '%Y') = '".$date."' GROUP BY name_channel, `user_id`) aggr");
        // }
        $chanalytics = DB::select("SELECT provider_id, name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.`provider_id`, youtube_analitic.name_channel, users.`unit`, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers` ORDER BY youtube_analitic.`subscribers` asc), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.provider_id, `user_id`) aggr ORDER BY `unit`");
        /* $ytanalytics = YoutubeAnalitic::select('youtube_analitic.*')
                                        ->join('users', 'users.id', '=', 'youtube_analitic.user_id')
                                        ->where(DB::raw("DATE_FORMAT(youtube_analitic.created_at,'%Y-%m')"),$date)
                                        ->where('users.is_active', 1)
                                        ->orderBy('youtube_analitic.id','asc')
                                        ->get(); */
        $gsheetlink = YoutubeAnaliticLink::where(DB::raw("Date_format(created_at, '%Y-%m')"),$date)->first();
        $typereporting[''] = 'Please Select';
        $typereporting[0] = 'Weekly';
        $typereporting[1] = 'Monthly';
        $typereporting[2] = 'Yearly';

        // return $gsheetlink;
        
        return view('youtube.analytics.index', [
            'date'          => $date,
            'chanalytics'   => $chanalytics,
            'gsheetlink'    => $gsheetlink,
            'typereporting' => $typereporting
        ]);
    }

    /*public function analyticJson(){
    	$class = new \stdClass();
    	$array_dsp = array();
        
        $date = date("Y-m");
        $var = "SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.`unit`, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers`), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`";
        
        $rows = DB::select($var);
		$countR = count($rows);
		$data = [];

		$data[0] = [
			$number[] = "No",
            $unit[] = "Unit",
            $name_channel[] = "Name Channel",
			$subscribers1[] = "Subscriber Week 1",
			$subscribers2[] = "Subscriber Week 2",
			$subscribers3[] = "Subscriber Week 3",
			$subscribers4[] = "Subscriber Week 4",
		];

        for($i=1; $i < $countR; $i++) { 
			if(empty(explode(',', $rows[$i]->subscribers)[0])) {$sub1=0;}
            else{$sub1= explode(',', $rows[$i]->subscribers)[0];}
            
            if(empty(explode(',', $rows[$i]->subscribers)[1])) {$sub2=0;}
            else{ $sub2= explode(',', $rows[$i]->subscribers)[1]; }
                        
            if(empty(explode(',', $rows[$i]->subscribers)[2])) {$sub3=0;}
            else{ $sub3= explode(',', $rows[$i]->subscribers)[2]; }

            if(empty(explode(',', $rows[$i]->subscribers)[3])) {$sub4=0;}
            else{ $sub4= explode(',', $rows[$i]->subscribers)[3]; }
            
			$data[] = [
				$number[] = $i,
                $unit[] = $rows[$i]->unit,
                $name_channel[] = $rows[$i]->name_channel,
				$subscribers1[] = $sub1,
				$subscribers2[] = $sub2,
				$subscribers3[] = $sub3,
				$subscribers4[] = $sub4,
			];
        }
        return ($data) ;
    }*/

    public function analyticJson(){
        $class = new \stdClass();
        $array_dsp = array();
        $num = 1;
        $date = date("Y-m");
        
        $data = [];

        $data[0] = [
            $number[] = "LIST Of Youtube MNC Group (All Channels)",
            $name_channel[] = "",
            $program[] = "",
            $subscribers1[] = "",
            $subscribers4[] = "",
            $increasing[] = "",
        ];

        $data[1] = [
            $number[] = "Period 26 May 2019 - 25 June 2019",
            $name_channel[] = " ",
            $program[] = "  ",
            $subscribers1[] = "  ",
            $subscribers4[] = "  ",
            $increasing[] = "  ",
        ];

        $data[2] = [
            $number[] = "No",
            $name_channel[] = "Youtube Account",
            $program[] = "Program",
            $subscribers1[] = "Subscriber Week #1",
            $subscribers4[] = "Subscriber Week #4",
            $increasing[] = "% Increasing Subs (W4 / W1)",
        ];

        $getUnit = DB::select("SELECT * FROM `users` GROUP BY unit ORDER BY unit");
        $countUnit = count($getUnit);


        for($x=1; $x < $countUnit; $x++) {

            $data[] = [
                $number[] = $getUnit[$x]->unit,
                $name_channel[] = "  ",
                $program[] = "  ",
                $subscribers1[] = "  ",
                $subscribers4[] = "  ",
                $increasing[] = "  ",
            ];

            $var = "SELECT * FROM ( SELECT youtube_analitic.name_channel, users.`unit`, users.program, `user_id`, CONCAT( GROUP_CONCAT(youtube_analitic.`subscribers` ORDER BY youtube_analitic.`subscribers` asc), ',' ) AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE DATE_FORMAT( youtube_analitic.created_at, '%Y-%m' ) = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id` ) aggr WHERE `unit` = '".$getUnit[$x]->unit."' ORDER BY `unit`";
            
            $rows = DB::select($var);
            $countR = count($rows);

            for($i=0; $i < $countR; $i++) { 
                if(empty(explode(',', $rows[$i]->subscribers)[0])) {$sub1=0;}
                else{$sub1= explode(',', $rows[$i]->subscribers)[0];}
                
                if(empty(explode(',', $rows[$i]->subscribers)[1])) {$sub2=0;}
                else{ $sub2= explode(',', $rows[$i]->subscribers)[1]; }
                            
                if(empty(explode(',', $rows[$i]->subscribers)[2])) {$sub3=0;}
                else{ $sub3= explode(',', $rows[$i]->subscribers)[2]; }

                if(empty(explode(',', $rows[$i]->subscribers)[3])) {$sub4=0;}
                else{ $sub4= explode(',', $rows[$i]->subscribers)[3]; }
                
                
                if($sub1!="0" && $sub2=="0" && $sub3=="0" && $sub4=="0"){
                    $incres=0;
                }elseif($sub1!="0" && $sub2!="0" && $sub3=="0" && $sub4=="0"){
                    $incres = number_format((float)(($sub2-$sub1)/$sub1)*100, 2, '.', '');
                }elseif($sub1!="0" && $sub2!="0" && $sub3!="0" && $sub4=="0"){
                    $incres = number_format((float)(($sub3-$sub2)/$sub2)*100, 2, '.', '');
                }elseif($sub1!="0" && $sub2!="0" && $sub3!="0" && $sub4!="0"){
                    $incres = number_format((float)(($sub4-$sub3)/$sub3)*100, 2, '.', '');
                }else{
                    $incres = 0;
                }

                $data[] = [
                    $number[] = $num,
                    $name_channel[] = $rows[$i]->name_channel,
                    $program[] = $rows[$i]->program,
                    $subscribers1[] = $sub1,
                    $subscribers4[] = $sub4,
                    $increasing[] = $incres,
                ];
                $num++;
            }
            $data[] = [
                $number[] = "",
                $name_channel[] = "  ",
                $program[] = "  ",
                $subscribers1[] = "  ",
                $subscribers4[] = "  ",
                $increasing[] = "  ",
            ];

            
        }


        return ($data) ;
    }

    public function csv_export($date)
    {
        $chanalytics = DB::select("SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.unit, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers` ORDER BY youtube_analitic.`subscribers` asc), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`");

        return view('youtube.analytics.export_csv',compact('chanalytics'));
    }

    public function excell_export($date)
    {
        $id='';
        $filename = "Reporting-Subscribers-Weekly_".date("Y-m-d");
      //$query = DB::select("SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.unit, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers`), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`");
        $query=DB::select("SELECT unit FROM `users` WHERE unit!='' GROUP By unit");
        $excel = Excel::create($filename, function($excel) use ($id,$query) {
            $excel->sheet('Sheet1', function($sheet) use ($id,$query) {
                $sheet->loadView('youtube.analytics.export_excel', [
                    'chanalytics' => $query
                ]);
            });
        });
        return $excel->export('xlsx');
    }


    public function export_print($date)
    {
        // return $date;

        $unit = DB::select("SELECT * FROM `users` GROUP BY unit ORDER BY unit");

        $chanalytics = DB::select("SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.unit, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers`), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`");

        return view('youtube.analytics.export_print',compact('unit','chanalytics','date'));
    }

    public function redirectToProvider($provider){
        if(!config("services.$provider")) abort('404');
        
        return Socialite::driver($provider)
            ->scopes([
                'https://www.googleapis.com/auth/youtube',
                'https://www.googleapis.com/auth/youtube.force-ssl',
                'https://www.googleapis.com/auth/youtube.readonly',
                'https://www.googleapis.com/auth/youtubepartner-channel-audit',
                'https://www.googleapis.com/auth/youtubepartner',
                'https://www.googleapis.com/auth/youtube.upload',
            ])
            ->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider);
        $getToken = $user->user();
        if($provider=="twitter"){ //Jika add AUTH twitter
            $redirect = Auth::user()->roles[0]->name;
            $UserToken  = new UserToken();
            $UserToken->user_id = Auth::user()->id;
            $UserToken->type  = 'twitter';
            $UserToken->expires_in  = '3600';
            $UserToken->access_token  = $getToken->token;
            $UserToken->token_secret  = $getToken->tokenSecret;
            $UserToken->refresh_token  = $getToken->token;
            $UserToken->save();

            Flash::success('Data has been success..');
            return redirect($redirect.'/platforms');

        }elseif(Auth::check()){
            $redirect = Auth::user()->roles[0]->name;
            $model = User::with('accessrole')->where('provider_id', $user->id)->orWhere('email', $user->nickname)->first();

            if( empty($model) && ($provider=="youtube") ){  //save user socialmedia YOUTUBE
                DB::table('user_token')->insert([
                    'user_id'=>Auth::user()->id,
                    'type'=>'youtube',
                    'access_token' => $user->accessTokenResponseBody['access_token'],
                    'expires_in' => $user->accessTokenResponseBody['expires_in'],
                    'scopes' => $user->accessTokenResponseBody['scope'],
                    'token_type' => $user->accessTokenResponseBody['token_type'],
                    'token_secret' => $user->token,
                    'refresh_token' => $user->refreshToken,
                ]);

                Flash::success('Data has been success..');
                return redirect($redirect.'/platforms');

            }else{
                /*Tambah akses role dari Auth sosial media*/
                $roless = $model->roles[0]->name;
                if($roless =="creator" || $roless =="songwriter" || $roless =="singer" || $roless =="influencer" ){
                    $authMaster =  Session::get('group_master');
                    $UserGroup  = new UserGroup();
                    $UserGroup->user_master = $authMaster->id;
                    $UserGroup->user_child  = $model->id;
                    $UserGroup->save();

                    if($UserGroup){
                        Flash::success('Data has been registered.');
                        Session::forget('user_group');

                        $updateGroup = UserGroup::where(['user_master'=>$authMaster->id])->get();
                        session(['user_group'=>$updateGroup]);
                        return redirect($redirect.'/addaccount');

                    }else{
                        Flash::error('Data has been registered.');
                        return redirect($redirect.'/addaccount');
                    }
                }else{
                    Flash::error('User cant to be Access.');
                    return redirect($redirect.'/addaccount');    
                }   
            }
            
        }else{
           /* Tambah akun baru dari Auth Youtube*/
            $authUser = $this->authLogin->findOrCreateUser($user, $provider);
            Auth::login($authUser, true);
            return redirect()->route('creators.validate', auth()->user()->provider_id);
        }
        
    }

    // public function cmslist()
    // {
    //     $auth = Auth::user();
    //     $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'Y']])->get()->count();
    //     if($auth->roles[0]->name == 'user'){
    //         if($youtubePartner == 0){
    //             return redirect(route('authentication.network'));
    //         }
    //     }

    //     $callbackUrl = 'http://starhits.com/auth/youtube/callback';
    //     $client = new \Google_Client();
    //     $client->setClientId(env('GOOGLE_CLIENT_ID'));
    //     $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
    //     $client->setScopes('https://www.googleapis.com/auth/youtube');
    //     $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
    //     $client->setRedirectUri($callbackUrl);
    //     $client->setAccessType('offline');
    //     $client->setPrompt('consent select_account');

    //     $cmslist = YoutubePartner::get();
    //     $usertoken = UserToken::where('user_id', $auth->id)->get();
    //     //return $cmslist;
    //     foreach ($cmslist as $cms){
    //         $ch = curl_init();

    //         curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/youtube/v3/channels?part=contentDetails&managedByMe=true&onBehalfOfContentOwner='.$cms->content_id.'&key='.env('GOOGLE_CLIENT_ID'));
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

    //         curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

    //         $headers = array();
    //         $headers[] = 'Authorization: '.$usertoken[0]->token_type.' '.$usertoken[0]->access_token;
    //         $headers[] = 'Accept: application/json';
    //         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //         $result = curl_exec($ch);
    //         if (curl_errno($ch)) {
    //             echo 'Error:' . curl_error($ch);
    //         }
    //         curl_close($ch);
    //         $contentID = json_decode($result);
    //         dd($contentID);
    //     }

    //     return view('youtube.cms.index', [
    //         'youtubePartner' => $youtubePartner,
    //         'cmslist' => $cmslist
    //     ]);
    // }

    public function cmslist()
    {   


        $auth = Auth::user();

        $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->first();

        $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'Y']])->get()->count();
        if($auth->roles[0]->name == 'user'){
            if($youtubePartner == 0){
                return redirect(route('authentication.network'));
            }
        }

        $callbackUrl = 'http://starhits.com/auth/youtube/callback';
        $client = new \Google_Client();
        $client->setClientId(env('GOOGLE_CLIENT_ID'));
        $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $client->setScopes('https://www.googleapis.com/auth/youtube');
        $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setRedirectUri($callbackUrl);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $client->setPrompt('consent select_account');

        if ($token) {
            $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
            $client->setAccessToken($token);

            // Refresh the token if it's expired.
            if ($client->isAccessTokenExpired()) {

                $authUrl = $client->createAuthUrl();
                header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));
                if (isset($_GET['code'])) {
                    $authCode = $_GET['code'];
                    // Exchange authorization code for an access token.
                    $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                    /*header('Location: ' . filter_var($this->redirectUri, 
                    FILTER_SANITIZE_URL));
                    if(!file_exists(dirname($this->tokenFile))) {
                        mkdir(dirname($this->tokenFile), 0700, true);
                    }*/
                    dd($accessToken);

                    //file_put_contents($this->tokenFile, json_encode($accessToken));
                }else{
                    exit('No code found');
                }

                //https://accounts.google.com/signin/oauth/danger?authuser=0&part=AJi8hANc5kkMZlNtym7mZPv7MNDVca4oUiEI7O0fANiw6WNjKbL5Od8uHn8nlQcwAfgHmzicvo_9vuJRKFW3Ii74XzcdzrZjWYTPMuW5BwFLpdmhesGI60mXqQ8P4YundKuQblifILCzjnuD47y8WogHFSSAiE0jTqflPwmkWXQStYhYLYFMhzhNS5PpER1Z7JvW1n4-Nu-jZB8-GJ3grhhauSD0H00vc7Oli9eAaafeNaFNU3MmpekGtKycdumdWHHLlNCgmvf6iXJOEs1_iwMkEdq1DNXVNcVphMn8Mn0Y-uzQzF8qpLDs9wUxJLQSNK2YdvgMeQzfugCQhIHEUm-0srwrjpR4dpB8DHs8Vbjv2wWqvSEb9IM4JHBAe7vcaS9CHQc_qUQLzQbNHkS3EoPslZRTpQFynNYlATml6GPn1Ec-e7_zglA&hl=en-GB&as=tFTwHGEqpo6qZtL3lIrk_A&rapt=AEjHL4Nw9xCE381AK5lFcma_VA5EIDqBBQtORe_5Dz5XkahgGrsR8wSW2G5lngXBDl0Izr91mllSLeHMGtriCM1Q7B_B0_LYxA#

                //http://starhits.com/auth/youtube/callback?code=4/lgENMiStTyRXUVMBh4zSYY1KQ9WIXrd4Ta8V5O6RW4eQq2LeqFMUxRsjYdLqPnU6hEtlZ9JL6MkIXTBDG-QQanw&scope=https://www.googleapis.com/auth/youtube%20https://www.googleapis.com/auth/yt-analytics.readonly
                $client->setAccessToken($token);
                // save refresh token to some variable
                //$refreshTokenSaved = $client->getRefreshToken();


                //$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                //$newAccessToken = $client->getAccessToken();

                // $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
                // $client->refreshToken($token['refresh_token']);
                // $accessToken = $client->getAccessToken();

                //dd($refreshTokenSaved);

                /*// update access token
                $client->fetchAccessTokenWithRefreshToken($refreshTokenSaved);

                // pass access token to some variable
                $accessTokenUpdated = $client->getAccessToken();

                // append refresh token
                $accessTokenUpdated['refresh_token'] = $refreshTokenSaved;

                //Set the new acces token
                $accessToken = $refreshTokenSaved;
                $client->setAccessToken($accessToken);
                
                dd($accessTokenUpdated);*/

            }else{
                return "true";
            }

        } else {
            header('Location: ' . $client->createAuthUrl());
            die();
        }

        

        $cmslist = YoutubePartner::get();
        $usertoken = UserToken::where('user_id', $auth->id)->get();
        //return $cmslist;
        foreach ($cmslist as $cms){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/youtube/v3/channels?part=contentDetails&managedByMe=true&onBehalfOfContentOwner='.$cms->content_id.'&key='.env('GOOGLE_CLIENT_ID'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

            $headers = array();
            $headers[] = 'Authorization: '.$usertoken[0]->token_type.' '.$usertoken[0]->access_token;
            $headers[] = 'Accept: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $contentID = json_decode($result);
            dd($contentID);
        }

       /// return $cmslist;

        return view('youtube.cms.index', [
            'youtubePartner' => $youtubePartner,
            'cmslist' => $cmslist
        ]);
    }

    public function revokeYt(){
        $usertoken = UserToken::where('user_id', $auth->id)->get();
        $client = new \Google_Client();
        $client->setClientId(env('GOOGLE_CLIENT_ID'));
        $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $client->setScopes('https://www.googleapis.com/auth/youtube');
        $client->setRedirectUri(env('GOOGLE_REDIRECT'));
        $client->setAccessType('offline');
        $client->setPrompt('consent select_account');
        $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->first();
        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
        //dd($token);
        $client->setAccessToken($token);
        $client->revokeToken($token);
    }

    public function authentication()
    {
        $auth = Auth::user();
        $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id]])->get()->count();

        if($auth->roles[0]->name == 'user'){
            if($youtubePartner>0){
                return redirect(route('connect.cms'));
            }
        }

        return view('youtube.cms.authentication', [
            'youtubePartner' => $youtubePartner
        ]);
    }

    public function selectcms()
    {
        $auth = Auth::user();
        $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'N']])->get()->count();
        $cmslist = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'N']])->get();

        if($auth->roles[0]->name == 'user'){
            if($youtubePartner == 0){
                return redirect(route('authentication.network'));
            }
        }

        return view('youtube.cms.select_cms', [
            'youtubePartner' => $youtubePartner,
            'cmslist' => $cmslist
        ]);
    }

    public function updateselectedcms(Request $request)
    {
        //dd($request->check);
        $auth = Auth::user();
        $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'N']])->get()->count();
        $cmslist = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'N']])->get();

        if($auth->roles[0]->name == 'user'){
            if($youtubePartner == 0){
                return redirect(route('authentication.network'));
            }
        }

        $idYP = $request->check;
        YoutubePartner::whereIn('id', $idYP)->update(array('selected' => 'Y'));
        return redirect(route('user.dashboard.index'));
    }

}
