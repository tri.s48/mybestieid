<?php

namespace App\Http\Controllers\Website;

use App\Model\Series;
use App\Model\User;
use App\Model\Video;
use App\Model\City;
use App\Model\Bank;
use App\Model\Country;
use App\Model\CategoryInst;
use App\Repositories\UserRepository;
use Khill\Duration\Duration;

class ErrorHandlerController extends Controller
{
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
    public function errorCode404()
    {
        dd('stop');
    }
}
