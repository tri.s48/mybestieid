<?php

namespace App\Http\Controllers\Website;

use Alert;
use Newsletter;
use App\Model\Page;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Http\Controllers\Website\Controller;
use App\Model\Channel;

class PageController extends Controller
{

    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function index()
    {
        return view('frontend.index');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function page($slug)
    {
        $data = Page::select('content')->where('slug', '=', $slug)->first();
        empty($data) ? abort(404) : '';

        return view('frontend.page', compact('data'));   
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function channel()
    {
        $data = Channel::select('title', 'slug', 'image', 'is_hd')->where(['is_active' => '1'])->get();

        return view('frontend.channel.index', compact('data'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function detailChannel($slug)
    {
        $data = Channel::select('title', 'image', 'content', 'attr_3')->where(['is_active' => '1'])->first();

        $recomendation = Channel::select('title', 'slug', 'image', 'is_hd')->where(['is_active' => '1'])->inRandomOrder()->get();
        $keyOtherRecomendation = array_search($slug, array_column($recomendation->toArray(), 'slug'));
        unset($recomendation[$keyOtherRecomendation]);
        $newOtherRecomendation = array_values($recomendation->toArray());    

        return view('frontend.channel.detail', compact('data', 'newOtherRecomendation'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function newsletterStore(Request $request)
    {
        if (!Newsletter::isSubscribed($request->subsc_email)) {
            Newsletter::subscribePending($request->subsc_email);

            Alert::info('Thanks for subscription.');
            return redirect()->back();
        }

        Alert::info('You are already subscribe!');
        return redirect()->back();
    }
}
