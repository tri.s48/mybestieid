<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateMasterSingerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($request->typemail ==1){
            $singer             =   $request->name_singer;
            $penyanyi           =   explode(',', $singer);
            $name_singer        =   str_replace(" ", ".", strtolower($penyanyi[0]));
            $email_singer   =   $name_singer.'@starhits.id';
            if(empty($request->password_singer)){
                $password_singer            =   123456789012;
            }else{
                $password_singer            =   $request->password_singer;
            }
            return [
                'name_singer'      => 'required|max:30',
                'password_singer'  => 'min:8|regex:/^(?=.*?[0-9]).{6,}$/'
            ];
        }else{
            $email_singer   =   strtolower($request->email_singer);
            if(empty($request->password_singer)){
                $password_singer            =   123456789012;
            }else{
                $password_singer            =   $request->password_singer;
            }
            return [
                'name_singer'      => 'required|max:30',
                'email_singer'     => 'required|email|unique:users,email',
                'password_singer'  => 'min:8|regex:/^(?=.*?[0-9]).{6,}$/'
            ];
        }
    }

    public function messages()
    {
        return [
            'name_singer.max:30'    => 'Maksimal 30 Character',
            'email_singer.email'   => 'Format Email Salah',
            'email_singer.unique'   => 'Email Sudah terdaftar',
            'password_singer.regex' => 'Harus ada Kombinasi Angka.'
        ];
    }
}
