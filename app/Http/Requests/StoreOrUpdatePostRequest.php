<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreOrUpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        if(request()->postType == 'manual'){
            return [
                'attr_1'    => 'required|integer',
                // 'title'     => 'required|max:125|regex:/^[a-zA-Z0-9\s]*$/',
                'title'     => 'required|max:125',
                'attr_6'    => ['max:60','regex:/^[a-zA-Z0-9_\-]*$/','nullable'],
                'image'     => 'image|mimes:jpg,png,jpeg|max:1000'
            ];
        }else{
            if (Auth::user()->roles->first()->name == 'admin') {
                return [
                    'created_by'    => 'required|integer',
                    'parent_id'     => 'required|integer',
                    'attr_6'        => ['max:60','regex:/^[a-zA-Z0-9_\-]*$/','nullable'],
                    // 'title'         => 'required|max:125|string|regex:/^[a-zA-Z0-9\s]*$/',
                    'title'         => 'required|max:125|string',
                    'image'         => 'image|mimes:jpg,png,jpeg|max:1000'
                ];
            } else {
                return [
                    'parent_id'     => 'required|integer',
                    'attr_6'        => ['max:60','regex:/^[a-zA-Z0-9_\-]*$/','nullable'],
                    // 'title'         => 'required|max:125|string|regex:/^[a-zA-Z0-9\s]*$/',
                    'title'         => 'required|max:125|string',
                    'image'         => 'image|mimes:jpg,png,jpeg|max:1000'
                ];
            }
        }
    }

    public function messages()
    {
        return [
            'attr_1.required'   => 'Channel field is required.',
            'attr_1.integer'   => 'Channel field value must be integer.',
            'created_by.required'        => 'Creator field is required.',
            'created_by.integer'        => 'Creator field must be integer.',
            'parent_id.required'         => 'Series field is required.',
            'parent_id.integer'         => 'Series field must be integer.',
            'attr_6.required'            => 'ID/URL Youtube field is required.',
            'attr_6.max'            => 'ID/URL Youtube field may not be greater than 60 characters.',
            'attr_6.regex'            => 'ID/URL Youtube format is invalid.',
        ];
    }
}
