<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class AccessRole extends Model
{

	protected $table    = 'access_role';
	public $timestamps	= false;
	protected $fillable = [
		'user_id', 'role_id'
	];
	//public $timestamps=false;

	public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Model\Role');
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }
}