<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Category extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table    = 'category';
	protected $fillable = ['parent', 'title', 'is_active'];

	protected $auditInclude = [
        'title',
        'content',
    ];

    public function parent() {
		return $this->belongsTo(static::class, 'parent', 'id');
	}
}
