<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class UserToken extends Model
{

	protected $table    = 'user_token';
	protected $fillable = [
		'user_id', 'access_token', 'refresh_token', 'type', 'created', 'token_type', 'expires_in', 'token_secret', 'scopes'
	];
    public $timestamps  = false;

}