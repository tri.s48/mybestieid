<?php

namespace App\Providers;

use App\DataTables\Util\Request;
use App\Model\Ads;
use App\Model\Channel;
use App\Model\Page;
use App\Model\SiteConfig;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {        
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('datatables.request', function () {
            return new Request;
        });
    }
}
