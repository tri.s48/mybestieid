@section('css')
    @include('layouts.backend.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%']) !!}

@section('scripts')
    @include('layouts.backend.datatables_js')
    {!! $dataTable->scripts() !!}
    <script>
        function changeRecommend(_id) {
            $.ajax({
                @if (auth()->user()->roles->first()->name === 'admin')
                    url: "{{ route('admin.channel.updateHD') }}",
                @elseif(auth()->user()->roles->first()->name === 'user')
                    url: "{{ route('user.channel.updateHD') }}",
                @else
                    url: "{{ route('creator.channel.updateHD') }}",
                @endif
                method: "POST",
                data: {
                    id: _id
                },
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function(response) {
                    console.log(response);
                    toastr.success("Mengubah HD Berhasil");
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }
        $(document).on('click', '.featured', function(e) {
            var id = $(this).attr('data-id');
            changeRecommend(id);
        });
    </script>
    @include('layouts.backend.datatables_limit')
@endsection
