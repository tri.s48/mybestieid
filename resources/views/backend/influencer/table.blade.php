@section('css')
    @include('layouts.backend.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%']) !!}

@section('scripts')
    @include('layouts.backend.datatables_js')
    {!! $dataTable->scripts() !!}
    @include('layouts.backend.datatables_limit')
@endsection
