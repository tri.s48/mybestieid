@extends('layouts.backend.crud')

@section('breadcrumb')
    @php
    if (
        auth()
            ->user()
            ->roles->first()->name == 'admin'
    ) {
        $route = 'masterlagu.index';
    } elseif (
        auth()
            ->user()
            ->roles->first()->name == 'legal'
    ) {
        $route = 'legal.masterlagu.index';
    } elseif (
        auth()
            ->user()
            ->roles->first()->name == 'anr'
    ) {
        $route = 'anr.masterlagu.index';
    } elseif (
        auth()
            ->user()
            ->roles->first()->name == 'creator'
    ) {
        $route = 'creator.masterlagu.index';
    } elseif (
        auth()
            ->user()
            ->roles->first()->name == 'singer'
    ) {
        $route = 'singer.masterlagu.index';
    } elseif (
        auth()
            ->user()
            ->roles->first()->name == 'legal'
    ) {
        $route = 'songwriter.masterlagu.index';
    }
    @endphp
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><i class="fa fa-file-audio-o"></i> Detail Master Lagu</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route($route) }}">Master Lagu</a>
                </li>
                <li class="active">
                    Detail Lagu <strong>{{ $masterlagu->track_title }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection
@section('contentCrud')
    @include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Detail Master Lagu</h5>
                    {{-- @if (auth()->user()->roles->first()->name == 'admin')
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('masterlagu.create')}}">Add New</a>
                    <a class="btn btn-primary pull-right" style="margin:-10px 5px 0 5px;" href="{{ route('masterlagu.export_excell') }}"><i class="fa fa-file-excel-o"></i>Export Excell</a>
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px;" href="{{ route('masterlagu.export_print') }}"><i class="fa fa-print"></i>Print</a>
                    @elseif(auth()->user()->roles->first()->name == 'legal')
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('legal.masterlagu.create')}}">Add New</a>
                    <a class="btn btn-primary pull-right" style="margin:-10px 5px 0 5px;" href="{{ route('legal.masterlagu.export_excell') }}"><i class="fa fa-file-excel-o"></i>Export Excell</a>
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px;" href="{{ route('legal.masterlagu.export_print') }}"><i class="fa fa-print"></i>Print</a>
                    @elseif(auth()->user()->roles->first()->name == 'singer')
                    <a class="btn btn-primary pull-right" style="margin:-10px 5px 0 5px;" href="{{ route('singer.masterlagu.export_excell') }}"><i class="fa fa-file-excel-o"></i>Export Excell</a>
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px;" href="{{ route('singer.masterlagu.export_print') }}"><i class="fa fa-print"></i>Print</a>
                    @elseif(auth()->user()->roles->first()->name == 'songwriter')
                    <a class="btn btn-primary pull-right" style="margin:-10px 5px 0 5px" href="{{ route('songwriter.masterlagu.export_excell') }}"><i class="fa fa-file-excel-o"></i>Export Excell</a>
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px;" href="{{ route('songwriter.masterlagu.export_print') }}"><i class="fa fa-print"></i>Print</a>
                    @endif --}}
                </div>
                <div class="ibox-content">
                    {{-- <form style="margin-bottom: 10px;" class="form form-inline" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                                {!! Form::select('name_master', $singers, null, ['class' => 'form-control', 'id' => 'name_master', 'onchange'=>'this.form.submit()']) !!}
                                        </div>
                                        <div class="form-group">
                                                {!! Form::select('album', $release_titles, null, ['class' => 'form-control', 'id' => 'album', 'onchange'=>'this.form.submit()']) !!}
                                        </div>
                                        <div class="form-group">
                                                {!! Form::select('judul', $titles, null, ['class' => 'form-control', 'id' => 'judul', 'onchange'=>'this.form.submit()']) !!}
                                        </div>
                                        <div class="form-group">
                                                {!! Form::select('name_master', $songwriters, null, ['class' => 'form-control', 'id' => 'name_master']) !!}
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-2">
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-default clear-filtering"><i class="fa fa-undo"></i> Clear</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Row -->
                            </div>
                                                    
                        </form> --}}
                    @include('backend.masterlagu.detail.lagu.table')
                </div>
            </div>
        </div>
    </div>
@endsection
