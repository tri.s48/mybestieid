<html>
<head>

<style>
body{
    margin: 10px;
}
table{
    width: 100%;;
    margin: 20px 0px;
}
table, th, td{
    border-collapse: collapse;
}
thead tr th{
    height: 50px;
}
.table-bordered table, .table-bordered th, .table-bordered td {
    border: 2px solid black;
}
.noborder table, .noborder th, .noborder td {
    border: none;
}
th, td {
    padding: 5px;
    /*text-align: left;
    text-align: center;*/
}
.signature{
    padding: 40px 50px;
}
.silver{
    background-color: silver;
}
 .number{
    text-align:right;
}

.font-12{
    font-size: 12px;
}
.font-14{
    font-size: 12px;
}
</style>
</head>
<body onload="print();">
    <div style="width:100%; text-align:center;">
        <img style="width:300px;" src="{{ asset('img/hitsrecords.png') }}">
        <h2 >PT. Suara Mas Abadi</h2>
        @php 
            $reporting_month = App\Model\Dsp::where(['id' => $getdetails->dsp_id])->first();
        @endphp
        <p>Periode {{ date('F Y',strtotime($reporting_month->reporting_month)) }} </p>
    </div>

    <table border="1">
        <tr style="font-size: 15px;">
            <td><center><b>FORMULIR PERMINTAAN PEMBAYARAN</b></center></td>
        </tr>
        <tr>
            <td>
                <table class="table" border="0"> <!-- table keterangan  -->
                    <tr class="font-14">
                        <td width="20%">Penyanyi</td>
                        <td>:</td>
                        <td width="50%" >
                            @php $countSing = 0;
                                $retSing = "";
                            @endphp             
                            @foreach($masterlagu->rolelagu as $key => $singer)
                                @if(!empty($singer->penyanyi))
                                    <?php 
                                        $retSing .= $singer->penyanyi->name_master." & ";
                                    ?>
                                @endif
                            @endforeach
                            <?php 
                                echo rtrim($retSing," & ");
                            ?>
                        </td>
                        <td rowspan="5"  >
                            <span style="font-size:14px; font-weight: bold;padding-top: -30px;">Catatan / Note :</span><br>
                            <span style="font-size: 12px;">
                            * <i>Exclude Pajak</i><br>
                            * <i>Print Date : {{ date('d F Y',strtotime(date("Y-m-d"))) }} </i>
                            </span>
                        </td>
                    </tr>
                    <tr class="font-14">
                        <td>Pencipta / Publishing</td>
                        <td>:</td>
                        <td>
                            @php $countSong = 0;
                                $ret="";
                            @endphp     
                            @foreach($masterlagu->rolelagu as $keys => $song)
                                @if(!empty($song->pencipta))
                                <?php 
                                    $ret .= $song->pencipta->name_master." & ";
                                ?>
                                @endif         
                            
                            @endforeach
                            <?php 
                                echo rtrim($ret," & ");
                            ?>
                        </td>
                    </tr>
                    <tr class="font-14">
                        <td>Judul Lagu </td>
                        <td>:</td>
                        <td>{{$masterlagu->track_title}}</td>
                    </tr>
                    <tr class="font-14">
                        <td>Album </td>
                        <td>:</td>
                        <td>{{$masterlagu->release_title}}</td>
                    </tr>
                    <tr class="font-14">
                        <td>Label</td>
                        <td>:</td>
                        <td>{{$masterlagu->label_name}}</td>
                    </tr>                    
                </table>

                @php
                    if(empty($table['table_sing'])){
                        $sing = ($net_revenue*$masterlagu->percentage_penyanyi);
                    }else{
                        $minTableSing = $table['table_bank']-$table['table_sing'];
                        $sing = ($net_revenue*$masterlagu->percentage_penyanyi)/$table['table_sing'];
                    }


                    if($table['table_song']==0){
                        $minTableSong =1;
                    }elseif(empty($table['table_song'])){
                        $song = ($net_revenue*$masterlagu->percentage_pencipta);
                    }else{
                        $minTableSong = $table['table_bank']-$table['table_song'];
                        $song = ($net_revenue*$masterlagu->percentage_pencipta)/$table['table_song'];
                    }

                   
                    $totsing = $net_revenue*$masterlagu->percentage_penyanyi;
                    $totsong = $net_revenue*$masterlagu->percentage_pencipta;
                    $total_revenue = $totsing+$totsong;
                @endphp

                {{--
                    <table style="" border="1" class="font-12">
                    <tr align="center" style="font-weight: bold;">
                        <td width="70%" >Description</td>
                        <td>Total Price (Rp)</td>
                    </tr>
                    @foreach ($masterlagu->rolelagu as $x => $value) 
                        @if(!empty($value->penyanyi) )
                            <tr>
                                <td>Revenue penyanyi ({{$value->penyanyi->name_master}})</td>
                                <td align="right">{{number_format((float)$sing,0)}}</td>
                            </tr>
                        @endif
                    @endforeach

                    @foreach ($masterlagu->rolelagu as $x => $value) 
                        @if(!empty($value->pencipta))
                            <tr>
                                <td>Revenue pencipta  ({{$value->pencipta->name_master}})</td>
                                <td align="right">{{number_format((float)$song,0)}}</td>
                            </tr>
                        @endif
                    @endforeach
                    <tr class="font-14" style="font-weight: bold;">
                        <td>Total</td>
                        <td align="right" >{{number_format((float)$total_revenue,0)}}</td>
                    </tr>
                </table>
                --}}

                <table class="" border="1"  ><!-- Keterangan Rekening bank -->
                    <tr style="font-weight: bold;border: 1px #000 solid;" align="center" class="font-14">
                        <td>Penyanyi</td>
                        @php
                            if($minTableSong >= 1 ){
                         }else{ @endphp
                            <td>Pencipta</td>
                        @php } @endphp
                    </tr>

                    @for($x=0; $x<$table['table_bank']; $x++)
                    <tr>
                        <td>
                            @foreach ($masterlagu->rolelagu as $x => $value) 
                            @if(!empty($value->penyanyi))
                                <table class="noborder" border="1"> 
                                    <tr class="font-12">
                                        <td >No.Rek</td>
                                        <td>: {{$value->penyanyi->bank_account_number}}</td>
                                    </tr>
                                    <tr class="font-12">
                                        <td>A/n</td>
                                        <td>: {{$value->penyanyi->bank_holder_name}}</td>
                                    </tr>
                                    <tr class="font-12">
                                        <td>Bank</td>
                                         @php $bank = DB::table('bank')->
                                        where(['id' => $value->penyanyi->bank_id])->first();
                                        @endphp
                                        <td>: {{$bank->name}}</td>
                                    </tr>
                                    <tr class="font-12">
                                        <td>NPWP</td>
                                        <td>: {{$value->penyanyi->npwp}}</td>
                                    </tr>
                                    <tr style="border: 1px #000 solid; font-weight: bold;" class="font-12">
                                        <td colspan="2">Revenue penyanyi ({{$value->penyanyi->name_master}}) : Rp. {{number_format((float)$sing,0)}},-</td>
                                    </tr>
                                     <tr style="border: 1px #000 solid;">
                                        <td>&nbsp;</td>
                                        <td></td>
                                    </tr>
                                    <tr style="border: 1px #000 solid;">
                                        <td>&nbsp;</td>
                                        <td></td>
                                    </tr>
                                </table>  
                            @endif
                            @endforeach

                            @for($a=0; $a< $minTableSing; $a++)
                            <table class="noborder" border="0"> 
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </table>
                            @endfor
                        </td>


                        @php
                            if($minTableSong >= 1 ){
                         }else{ @endphp
                            
                        <td>
                            @php $numb=0; @endphp
                            @foreach ($masterlagu->rolelagu as $x => $value) 
                            @if(!empty($value->pencipta) )
                                <table class="noborder" border="1"> 
                                    <tr class="font-12">
                                        <td width="20%">No.Rek </td>
                                        <td>: {{$value->pencipta->bank_account_number}}</td>
                                    </tr>
                                    <tr class="font-12">
                                        <td>A/n</td>
                                        <td>: {{$value->pencipta->bank_holder_name}}</td>
                                    </tr>
                                    <tr class="font-12">
                                        <td>Bank</td>
                                         @php $bank = DB::table('bank')->
                                        where(['id' => $value->pencipta->bank_id])->first();
                                        @endphp
                                        <td>: {{$bank->name}}</td>
                                    </tr>
                                    <tr class="font-12">
                                        <td>NPWP</td>
                                        <td>: {{$value->pencipta->npwp}}</td>
                                    </tr>
                                    <tr class="font-12" style="border: 1px #000 solid;font-weight: bold;">
                                        <td colspan="2">Revenue pencipta ({{$value->pencipta->name_master}}) : Rp. {{number_format((float)$song,0)}},-</td>
                                    </tr>
                                     <tr style="border: 1px #000 solid;">
                                        <td>&nbsp;</td>
                                        <td></td>
                                    </tr>
                                    <tr style="border: 1px #000 solid;">
                                        <td>&nbsp;</td>
                                        <td></td>
                                    </tr>
                                </table>
                                
                            @endif
                            @endforeach

                            @for($b=0; $b< $minTableSong; $b++)
                            <table class="noborder" border="1"> 
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr class="font-12">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </table>
                            @endfor
                        </td>
                        @php } @endphp
                    </tr>
                    @endfor

                    {{--
                        <tr class="font-12" style="font-weight: bold;">
                            @foreach ($masterlagu->rolelagu as $value) 
                            @if(!empty($value->penyanyi))
                                <td>Revenue penyanyi ({{$value->penyanyi->name_master}}) : Rp. {{number_format((float)$sing,0)}},-</td>
                            @endif
                            @endforeach

                            @php $i = 0; @endphp
                            @foreach ($masterlagu->rolelagu as $value) 
                            @if(!empty($value->pencipta))
                                <td>Revenue pencipta ({{$value->pencipta->name_master}}) : Rp. {{number_format((float)$song,0)}},-</td>
                            @endif
                            @endforeach
                        </tr>    

                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                    </tr>--}}
                    
                    
                </table>
            </td>
        </tr>
    </table>


    

    <section >
        <div class="form-group">
            <div class="col-sm-12 table-responsive">
                
                
                <table style="width: 100%; font-size: 15px; text-align: center;" border="1">
                    <tr>
                        <td>Prepared By</td>
                        <td>Reviewed By</td>
                        @if($net_revenue <= 5000000)
                            <td colspan="2">Approved By</td>
                        @else
                            <td colspan="3">Approved By</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="signature" width="150"><i style="font-size: 11px;">Print Date : {{ date('d F Y',strtotime(date("Y-m-d"))) }} </i></td>
                        <td class="signature"></td>
                        <td class="signature"></td>
                        @if($net_revenue > 5000000)
                            <td class="signature"></td>
                        @endif
                    </tr>
                    <tr style="font-weight: bold;font-size: 14px;">
                        <td><i style="font-size: 11px;">Print by System Starhits</i></td>
                        <td>Head Of Finance</td>
                        <td>CFO</td>
                        @if($net_revenue > 5000000)
                            <td>Director</td>
                        @endif
                    </tr>
                </table>

            </div>
        </div>
    </section>
</body>
</html>