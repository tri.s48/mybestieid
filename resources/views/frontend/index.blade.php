@extends('layouts.frontend')

@section('content')
    <div class="row content-section">
        <div class="col-sm-12 col-md-12">
            <div id="myCarousel" class="carousel slide w-100" data-ride="carousel">
                <ol class="carousel-indicators float-right">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner w-100" role="listbox">
                    <div class="carousel-item active">
                        <div class="col-xxl-5 col-lg-8 col-md-12">
                            <img height="370" width="750" class="img-fluid"
                                src="#">
                            <a href="#" class="carousel-detail">
                                <div>
                                    <!-- <h3>Breaking bad <span><i class="fa fa-star"></i> 6.1/10</span></h3>
          <p>Frustrated by the constant quarrel between the members of his dysfunctional family, Max
            loses interest to celebrate Christmas, awakening Krampus, a demon who wi...</p> -->
                                    <button><i class="fa fa-play"></i> <span>Watch
                                            Now<span></button>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="col-xxl-5 col-lg-8 col-md-12">
                            <img height="370" width="750" class="img-fluid"
                                src="#">
                            <a href="/serie/3/just-me-and-you.html" class="carousel-detail">
                                <div>
                                    <!-- <h3>The Final Season <span><i class="fa fa-star"></i> 8.3/10</span></h3>
          <p>The survivors of a plane crash find themselves stranded on a mysterious island. They are
            forced to work together for their survival when they realise that they...</p> -->
                                    <button><i class="fa fa-play"></i> <span>Watch
                                            Now<span></button>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 ">
            <a href="/channels/all/all/views.html" class="title-more">
                <span>Featured LiveTV</span>
                <i class="fa fa-th float-right"></i>
            </a>
        </div>
        <div class="flix-carousel">
            <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
            <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
            <div class="flix-scroll-x">

                <a class="channel " href="/channel/40/vir-h1.html">
                    <img height="75" width="150"
                        src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/93ae4fbe1f6ac1f1d7bd9f0cb4233fc5.png" />
                    <div>
                        <span>HD</span>
                        <p class="two">LIVE</p>
                    </div>
                </a>
                
            </div>
        </div>
        <div class="col-sm-12 col-md-12 ">
            <a href="/actors.html" class="title-more">
                <span>Popular Actors</span>
                <i class="fa fa-th float-right"></i>
            </a>
        </div>
        <div class="flix-carousel">
            <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
            <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
            <div class="flix-scroll-x">

                <a href="profile-actor.html" class="actor">
                    <img src="#" />
                    <span>Demian</span>
                </a>

            </div>
        </div>
        <div class="col-sm-12 col-md-12 ">
            <a href="/filter/all/rating.html" class="title-more">
                <span>Top Rated</span>
                <i class="fa fa-th float-right"></i>
            </a>
        </div>
        <div class="flix-carousel">
            <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
            <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
            <div class="flix-scroll-x">

                <a href="/serie/27/green-space.html" title="Green space" class="poster ">
                    <div>
                        <img width="170" height="255" src="#" />
                        <div>
                        </div>
                    </div>
                </a>
               
            </div>
        </div>
        <div class="col-sm-12 col-md-12 ">
            <a href="/filter/all/views.html" class="title-more">
                <span>Popular</span>
                <i class="fa fa-th float-right"></i>
            </a>
        </div>
        <div class="flix-carousel">
            <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
            <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
            <div class="flix-scroll-x">

                <a href="/serie/4/breaking-bad.html" title="Breaking Bad" class="poster ">
                    <div>
                        <img width="170" height="255"
                            src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/0839d912a6fe9ac2ce8c27c713831bef.jpg" />
                        <div>
                            <span>EP16</span>
                            <p class="two">HD</p>
                        </div>
                    </div>
                </a>
                
            </div>
        </div>
        <div class="col-sm-12 col-md-12 ">
            <a href="/filter/Action/newest.html" class="title-more">
                <span>Action</span>
                <i class="fa fa-th float-right"></i>
            </a>
        </div>
        <div class="flix-carousel">
            <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
            <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
            <div class="flix-scroll-x">

                <a href="/serie/4/breaking-bad.html" title="Breaking Bad" class="poster ">
                    <div>
                        <img width="170" height="255"
                            src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/0839d912a6fe9ac2ce8c27c713831bef.jpg" />
                        <div>
                            <span>EP16</span>
                            <p class="two">HD</p>
                        </div>
                    </div>
                </a>
                
            </div>
        </div>
        <div class="col-sm-12 col-md-12 ">
            <a href="/filter/Adventure/newest.html" class="title-more">
                <span>Adventure</span>
                <i class="fa fa-th float-right"></i>
            </a>
        </div>
        <div class="flix-carousel">
            <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
            <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
            <div class="flix-scroll-x">

                <a href="/serie/3/just-me-and-you.html" title="Just me and you" class="poster ">
                    <div>
                        <img width="170" height="255"
                            src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8af488ad2fc117d2621fbf4e3568860a.jpg" />
                        <div>
                        </div>
                    </div>
                </a>
               
            </div>
        </div>
        <div class="col-sm-12 col-md-12 ">
            <a href="/filter/Crime/newest.html" class="title-more">
                <span>Crime</span>
                <i class="fa fa-th float-right"></i>
            </a>
        </div>
        <div class="flix-carousel">
            <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
            <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
            <div class="flix-scroll-x">

                <a href="/serie/4/breaking-bad.html" title="Breaking Bad" class="poster ">
                    <div>
                        <img width="170" height="255"
                            src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/0839d912a6fe9ac2ce8c27c713831bef.jpg" />
                        <div>
                            <span>EP16</span>
                            <p class="two">HD</p>
                        </div>
                    </div>
                </a>
                
            </div>
        </div>
        <div class="col-sm-12 col-md-12 ">
            <a href="/filter/Fantasy/newest.html" class="title-more">
                <span>Fantasy</span>
                <i class="fa fa-th float-right"></i>
            </a>
        </div>
        <div class="flix-carousel">
            <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
            <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
            <div class="flix-scroll-x">

                <a href="/serie/3/just-me-and-you.html" title="Just me and you" class="poster ">
                    <div>
                        <img width="170" height="255"
                            src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8af488ad2fc117d2621fbf4e3568860a.jpg" />
                        <div>
                        </div>
                    </div>
                </a>
                
            </div>
        </div>
        <div class="col-sm-12 col-md-12 ">
            <a href="/filter/Family/newest.html" class="title-more">
                <span>Family</span>
                <i class="fa fa-th float-right"></i>
            </a>
        </div>
        <div class="flix-carousel">
            <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
            <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
            <div class="flix-scroll-x">

                <a href="/serie/3/just-me-and-you.html" title="Just me and you" class="poster ">
                    <div>
                        <img width="170" height="255"
                            src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8af488ad2fc117d2621fbf4e3568860a.jpg" />
                        <div>
                        </div>
                    </div>
                </a>
                
            </div>
        </div>
    </div>
@endsection
