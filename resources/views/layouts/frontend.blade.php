<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <title>{{ webConfig()['website_title'] }}</title>
    <meta property="description" content="">
    <meta property="keywords" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:site_name" content=" ">
    <meta property="og:image" content="">
    <meta property="og:url" content="/" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/017eaf7e3f.js" crossorigin="anonymous"></script>
</head>
<body class="dark-mode">
    <div class="container-fluid">
        <div class="row main-row">
            <div class="sidebar">
                @include('layouts.frontend.sidebar')
            </div>
            <div class="close-menu-left-btn"></div>
            <div class="main">
                @include('layouts.frontend.header')
                <div class="content">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    @include('layouts.frontend.g-play')
    <div class="background-close login-full">
        <div class="login-box">
            @include('frontend.auth.login')
        </div>
    </div>
    <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/js/popper.min.js') }}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="{{ asset('frontend/js/app.js') }}"></script>
</body>
</html>
