<div class="gplay-box">
    <span><i class="fas fa-times-circle"></i></span>
    <div>
        <img src="{{ webConfig()['website_logo_header'] }}">
        <h3>Bestie App</h3>
        <p>{{ webConfig()['website_slogan'] }}</p>
    </div>
    <a href="https://play.google.com/store/apps/details?id=com.virmana.flix" target="_blank" class="btn">
        <i class="fas fa-download"></i> 
        Download
    </a>
</div>
