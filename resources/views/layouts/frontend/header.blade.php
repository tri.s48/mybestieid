<div class="xs-header">
    <button class="btn btn-primary float-left menu-left-btn"><i class="fas fa-bars"></i></button>
    <img src="favicon.png">
    <button class="btn btn-primary float-right open-login"><i class="fas fas fa-user"></i></button>
    <button class="btn btn-primary float-right search-btn"><i class="fas fa-search"></i></button>
</div>
<div class="row top-nav">
    <div class="col-sm-3 col-md-3 ">
        <div class="tab-title">
            @if (!empty(pageTitle(request()->segment(2))->title))
                {{ pageTitle(request()->segment(2))->title }}
            @else
                {{ menuTitle(request()->segment(1)) }}
            @endif
        </div>
    </div>
    <div class="col-sm-6 col-md-6 search-form">
        <form action="/search" class="navbar-form" role="search">
            <div class="search-group">
                <i class="fas fa-search"></i>
                <input type="text" class="form-control" placeholder="Movies,Tv Shows , Tv Channels , Actors ..."
                    name="q">
                <button class="btn btn-default" type="submit">Search</button>
            </div>
        </form>
        <button class="btn btn-primary float-right for-phone search-btn-close"><i class="fas fa-times"></i></button>
    </div>
    <div class="col-sm-3 col-md-3 account-action for-desktop">
        <div class="float-right">
            <a href="/register/" class="btn btn-link"> Sign-up </a>
            <a href="/login" class="btn btn-primary open-login">Sign-in </a>
        </div>
    </div>
</div>
