<h2 class="page-header logo-app">
    <img src="{{ webConfig()['website_logo_header'] }}">
</h2>
<div class="nav-bar-container">
    <nav class="navbar bg-light left-navbar">
        <ul class="navbar-nav">
            <a href="#" class="btn  btn-subscription">
                <i class="fas fa-star"></i> 
                Subscribe now !
            </a>
            
            @if (!empty(menu()))    
                @foreach (menu() as $menu)    
                <li class="nav-item  {{ Request::segment(1) == $menu['url'] ? 'active' : '' }} ">
                    <a class="nav-link" href="{{ route('home').'/'.$menu['url'] }}"> 
                        <i class="{{ $menu['icon'] }}"></i> 
                        {{ $menu['title'] }}
                    </a>
                </li class="nav-item">
                @endforeach
            @endif

            <li class="nav-item ">
                <a class="nav-link open-login " href="/mylist.html">
                    <i class="fas fa-folder"></i>
                    My list
                </a>
            </li>
        </ul>
    </nav>
</div>
<nav class="navbar bg-light left-navbar bottom-navbar">
    <a id="lightmode" href="#"><i class="fas fa-sun"></i></a>
    <a id="darkmode" href="#" style="display:none"><i class="fas fa-moon"></i> </a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-envelope"></i>
                Contact us
            </a>
        </li class="nav-item">
        
        @if (!empty(page()))  
            @foreach (page() as $page)
            <li class="nav-item">
                <a class="nav-link" href="{{ route('page.slug', $page['slug']) }}">
                    <i class="{{ $page['attr_3'] }}"></i> 
                    {{ $page['title'] }}
                </a>
            </li class="nav-item">
            @endforeach
        @endif

    </ul>
</nav>
