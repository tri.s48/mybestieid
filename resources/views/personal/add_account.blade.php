@extends('layouts.backend.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Add Account</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins ">
                <div class="ibox-title">
                    <h5>Add Account</h5>
                </div>
                <div class="ibox-content" style="height: 400px">
                    <div class=" text-center col-md-6 col-md-offset-3">
                        <br><br>
                    <form id="form-login" class="form-horizontal" method="POST" action="{{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/post/addaccount">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="text" class="form-control required" name="email" value="{{ old('email') }}" placeholder="Email" autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control required" name="password" placeholder="Password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                     
                        <br>
                        <hr>

                        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                    </form>
                </div>
                </div>
                
            </div>
        </div>
    </div>


@endsection
