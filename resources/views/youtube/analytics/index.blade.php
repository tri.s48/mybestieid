@extends('layouts.backend.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-file-audio-o"></i> Youtube Analytics</h2>
        <ol class="breadcrumb">
            <li class="active">
                <a href="#">Youtube Analytics</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Youtube Analytics</h5>
                    {{-- @if(auth()->user()->roles->first()->name == 'admin')
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('mastersinger.create')}}">Add New</a>
                    @elseif(auth()->user()->roles->first()->name == 'legal')
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('legal.mastersinger.create')}}">Add New</a>
                    @endif --}}
                </div>
                <div class="ibox-content">
                    {!! Form::open(['route' => ['youtube.analytics.detail'], 'files' => true, 'class' => 'form-horizontal', 'id' => 'analyticsform']) !!}
                    {{-- <div class="col-md-3 col-sm-12" style="padding:20px 0;">
                        {!! Form::label('Type Reporting', '', ['class' => 'col-sm-4 control-label', 'style' => 'text-align:left!important;']) !!}
                        <div class="col-md-8 col-sm-8" >
                            {!! Form::select('typereporting', $typereporting, '',['class' => 'form-control', 'id'=>'typereporting']) !!}
                        </div>
                    </div> --}}
                    <div id="selectmonth" class="col-md-3 col-sm-12" style="padding:20px 0; display:block;">
                        {!! Form::label('Month', '', ['class' => 'col-sm-4 text-left control-label']) !!}
                        <div class="col-md-8 col-sm-8" >
                            {!! Form::text('month', '', ['class' => 'form-control monthpicker', 'autocomplete'=>'off', 'id' => 'month']) !!}
                        </div>
                    </div>
                    <div id="selectyear" class="col-md-3 col-sm-12" style="padding:20px 0; display:block;">
                        {!! Form::label('Year', '', ['class' => 'col-sm-4 text-left control-label']) !!}
                        <div class="col-md-8 col-sm-8" >
                            {!! Form::text('year', '', ['class' => 'form-control yearpicker', 'autocomplete'=>'off', 'id' => 'year']) !!}
                        </div>
                    </div>
                    <!-- Submit Field -->
                    <div class="col-md-3 col-sm-12 text-center" style="padding:20px 0;">
                        {!! Form::submit( 'Process', ['class' => 'btn btn-success', 'name' => 'submitbutton', 'value' => 'save'])!!}
                        <a href="{!! route('masterlagu.index') !!}" class="btn btn-default">Back</a>
                    </div>
                    @include('youtube.analytics.table')
                </div>
            </div>
        </div>
    </div>
@endsection


