<?php $__env->startSection('css'); ?>
    <?php echo $__env->make('layouts.backend.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo e(Form::hidden('type', 'channel')); ?>


<!-- Type Field -->
<div class="form-group">
    <?php echo Form::label('type', 'Type:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::select('type', ['join' => 'Join', 'partnership' => 'Partnership', 'page' => 'Page'], null, ['placeholder' => 'Select Type', 'class' => 'form-control', 'required' => true]); ?>

    </div>
</div>

<div class="form-group parent">
    <?php echo Form::label('parent_id', 'Parent:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::select('parent_id', $pageList, isset($page) && $page->parent_id ? $page->parent_id : 'parent', ['placeholder' => 'None', 'class' => 'form-control m-b']); ?>

    </div>
</div>
<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('title', null, ['class' => 'form-control', 'required' => true, 'maxlength' => 100]); ?>

    </div>
</div>

<div class="form-group">
    <?php echo Form::label('icon', 'Icon:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('attr_3', null, ['class' => 'form-control', 'required' => true, 'maxlength' => 100]); ?>

    </div>
</div>

<!-- Sub Title Field -->
<div class="form-group subtitle">
    <?php echo Form::label('subtitle', 'Sub Title:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('subtitle', null, ['class' => 'form-control', 'maxlength' => 100]); ?>

    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    <?php echo Form::label('is_active', 'Status:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::select('is_active', ['1' => 'Active', '0' => 'Inactive'], null, ['placeholder' => 'Select Status', 'class' => 'form-control', 'required' => true]); ?>

    </div>
</div>

<!-- Content Field -->
<div class="form-group content">
    <?php echo Form::label('content', 'Content:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::textarea('content', null, ['class' => 'form-control']); ?>

    </div>
</div>

<!-- Picture Field -->
<div class="form-group image">
    <?php echo Form::label('image', 'Picture:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
        <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

        <a href="<?php echo route('page.index'); ?>" class="btn btn-default">Cancel</a>
    </div>
</div>

<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/piexif.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/sortable.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/purify.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/fileinput.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/tinymce/jquery.tinymce.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/tinymce/tinymce.min.js')); ?>"></script>
    <script>
        var formType = '<?php echo e($formType); ?>';
        $("#photo").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-photo-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="<?php echo e($formType == 'edit' && $page->image ? asset($page->image) : asset('img/default_avatar_male.jpg')); ?>" alt="Your icon" style="width:160px">',
            layoutTemplates: {
                main2: '{preview} {remove} {browse}'
            },
            allowedFileExtensions: ["jpg", "png", "jpeg"]
        });

        tinymce.init({
            selector: "textarea",
            file_picker_types: 'image',
            images_upload_credentials: true,
            automatic_uploads: false,
            theme: "modern",
            paste_data_images: true,
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime nonbreaking save table contextmenu directionality",
                "template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor",
            image_advtab: true,
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');

                // Note: In modern browsers input[type="file"] is functional without 
                // even adding it to the DOM, but that might not be the case in some older
                // or quirky browsers like IE, so you might want to add it to the DOM
                // just in case, and visually hide it. And do not forget do remove it
                // once you do not need it anymore.

                input.onchange = function() {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function() {
                        // Note: Now we need to register the blob in TinyMCEs image blob
                        // registry. In the next release this part hopefully won't be
                        // necessary, as we are looking to handle it internally.
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);

                        // call the callback and populate the Title field with the file name
                        cb(blobInfo.blobUri(), {
                            title: file.name
                        });
                    };
                };

                input.click();
            }
        });

        $('select[name="type"]').change(function() {
            if ($(this).val() == "join" || $(this).val() == "partnership") {
                $('.parent').hide();
                $('.subtitle').hide();
                $('.content').hide();
                $('.image').show();
            } else {
                $('.parent').show();
                $('.subtitle').show();
                $('.content').show();
                $('.image').hide();
            }
        });

        var type = $('#type').val();
        if (type == "join" || type == "partnership") {
            $('.parent').hide();
            $('.subtitle').hide();
            $('.content').hide();
            $('.image').show();
        } else {
            $('.parent').show();
            $('.subtitle').show();
            $('.content').show();
            $('.image').hide();
        }

        // $(document).ready(function(){
        //     var type = $('#type').val();
        //     if (type == "join" || type == "partnership") {
        //         $('[name="parent_id"]').prop('disabled', true);
        //         $('[name="subtitle"]').prop('disabled', true);
        //         tinyMCE.get('content').getBody().setAttribute('contenteditable', false);
        //     }
        // });
    </script>
<?php $__env->stopSection(); ?>
