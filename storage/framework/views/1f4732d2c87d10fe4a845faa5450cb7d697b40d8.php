
<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('css/fileinput.min.css')); ?>" rel="stylesheet">
    <style>
        .kv-photo .krajee-default.file-preview-frame,
        .kv-photo .krajee-default.file-preview-frame:hover {
            margin: 0;
            padding: 0;
            border: none;
            box-shadow: none;
            text-align: center;
        }

        .kv-photo .file-input {
            display: table-cell;
            max-width: 220px;
        }

        .kv-reqd {
            color: red;
            font-family: monospace;
            font-weight: normal;
        }

    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumb'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Page</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('page.index')); ?>">Page</a>
                </li>
                <li class="active">
                    <strong>Edit</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentCrud'); ?>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Page</h5>
        </div>
        <div class="ibox-content">
            <?php echo Form::model($page, ['route' => ['page.update', $page->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]); ?>

            <?php echo $__env->make('backend.page.fields', ['formType' => 'edit'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.backend.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>