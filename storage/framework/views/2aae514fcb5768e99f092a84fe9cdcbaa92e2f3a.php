<?php if(auth()->user()->roles->first()->name === 'admin'): ?>
<?php echo Form::open(['route' => ['admin.serie.destroy', $id], 'method' => 'delete']); ?>

<div class='btn-group'>
    <a href="<?php echo e(route('admin.serie.show', $id)); ?>" class='btn btn-info btn-xs' title="Show Series">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="<?php echo e(route('admin.serie.edit', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Series">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    <?php if($is_active == 1): ?>
    <?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-warning btn-xs',
        'title' => 'Inactive Series',
        'onclick' => "return confirm('Do you want to inactive this series?')",
        'name' => 'action',
        'value' => 'inact'
    ]); ?>

    <?php else: ?>
    <?php echo Form::button('<i class="glyphicon glyphicon-ok"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-success btn-xs',
        'title' => 'Activated Series',
        'onclick' => "return confirm('Do you want to activated this series?')",
        'name' => 'action',
        'value' => 'act'
    ]); ?>

    <?php endif; ?>
    <?php echo Form::button('<i class="glyphicon glyphicon-remove"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'title' => 'Delete Series',
        'onclick' => "return confirm('Do you want to delete this posts?')",
        'name' => 'action',
        'value' => 'del'
    ]); ?>

</div>
<?php echo Form::close(); ?>

<?php elseif(auth()->user()->roles->first()->name === 'user'): ?>
<?php echo Form::open(['route' => ['user.serie.destroy', $id], 'method' => 'delete']); ?>

<div class='btn-group'>
    <a href="<?php echo e(route('user.serie.show', $id)); ?>" class='btn btn-info btn-xs' title="Show Series">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="<?php echo e(route('user.serie.edit', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Series">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    <?php if($is_active == 1): ?>
    <?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-warning btn-xs',
        'title' => 'Inactive Series',
        'onclick' => "return confirm('Do you want to inactive this series?')",
        'name' => 'action',
        'value' => 'inact'
    ]); ?>

    <?php else: ?>
    <?php echo Form::button('<i class="glyphicon glyphicon-ok"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-success btn-xs',
        'title' => 'Activated Series',
        'onclick' => "return confirm('Do you want to activated this series?')",
        'name' => 'action',
        'value' => 'act'
    ]); ?>

    <?php endif; ?>
    
</div>
<?php echo Form::close(); ?>

<?php else: ?>
<?php echo Form::open(['route' => ['creator.serie.destroy', $id], 'method' => 'delete']); ?>

<div class='btn-group'>
    <a href="<?php echo e(route('creator.serie.show', $id)); ?>" class='btn btn-info btn-xs' title="Show Series">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="<?php echo e(route('creator.serie.edit', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Series">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    <?php if($is_active == 1): ?>
    <?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-warning btn-xs',
        'title' => 'Inactive Series',
        'onclick' => "return confirm('Do you want to inactive this series?')",
        'name' => 'action',
        'value' => 'inact'
    ]); ?>

    <?php else: ?>
    <?php echo Form::button('<i class="glyphicon glyphicon-ok"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-success btn-xs',
        'title' => 'Activated Series',
        'onclick' => "return confirm('Do you want to activated this series?')",
        'name' => 'action',
        'value' => 'act'
    ]); ?>

    <?php endif; ?>
    
</div>
<?php echo Form::close(); ?>

<?php endif; ?>