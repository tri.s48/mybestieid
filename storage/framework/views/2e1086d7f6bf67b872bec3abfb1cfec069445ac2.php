

<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-youtube"></i> Youtube Revenue</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Youtube Revenue</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
            
            <?php if(auth()->user()->hasRole(['finance'])): ?>
                <div class="ibox-title">
                    <h5>Youtube Revenue</h5>
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="<?php echo e(route('finance.revenue.create')); ?>">Add New</a>
                </div>
            <?php elseif(auth()->user()->hasRole('admin')): ?>
                <div class="ibox-title">
                    <h5>Youtube Revenue</h5>
                    <a href="<?php echo e(route('revenue.download.template')); ?>" style="margin-top: -10px;margin-bottom: 5px" class="btn btn-primary pull-right" id="btn-template">Download Template</a>
                </div>
            <?php endif; ?>

                <div class="ibox-content">
                    <form style="margin-bottom: 10px;" class="form-horizontal" role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Filter By</label>
                                        <div class="col-md-4">
                                            <?php echo Form::select('month', $months, null, ['class' => 'form-control', 'id' => 'month']); ?>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-default clear-filtering"><i class="fa fa-undo"></i> Clear Filtering</button>
                                        <button type="button" class="btn btn-success btn-search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /Row -->
                        </div>
                                                
                    </form>
                    <?php echo $__env->make('revenue.table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-revision" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center">Want to edit manual or reupload file</h4>
                </div>
                <div class="modal-body text-center">
                    <?php echo Form::open(['id' => 'form-revision','method' => 'get' ]); ?>

                    <input type="hidden" name="edit_status" id="edit-status">
                    <button type="submit" class="btn btn-warning" id="btn-upload">Reupload File</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>