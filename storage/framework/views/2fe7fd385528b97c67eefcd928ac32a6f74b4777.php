

<?php $__env->startSection('breadcrumb'); ?>
    <?php
    if (
        auth()
            ->user()
            ->roles->first()->name == 'admin'
    ) {
        $route = 'masterlagu.index';
    } elseif (
        auth()
            ->user()
            ->roles->first()->name == 'legal'
    ) {
        $route = 'legal.masterlagu.index';
    } elseif (
        auth()
            ->user()
            ->roles->first()->name == 'anr'
    ) {
        $route = 'anr.masterlagu.index';
    } elseif (
        auth()
            ->user()
            ->roles->first()->name == 'creator'
    ) {
        $route = 'creator.masterlagu.index';
    } elseif (
        auth()
            ->user()
            ->roles->first()->name == 'singer'
    ) {
        $route = 'singer.masterlagu.index';
    } elseif (
        auth()
            ->user()
            ->roles->first()->name == 'legal'
    ) {
        $route = 'songwriter.masterlagu.index';
    }
    ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><i class="fa fa-file-audio-o"></i> Detail Master Lagu</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route($route)); ?>">Master Lagu</a>
                </li>
                <li class="active">
                    Detail Lagu <strong><?php echo e($masterlagu->track_title); ?></strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Detail Master Lagu</h5>
                    
                </div>
                <div class="ibox-content">
                    
                    <?php echo $__env->make('backend.masterlagu.detail.lagu.table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.backend.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>