

<?php $__env->startSection('content'); ?>
<div class="row content-section">
    <div class="filter-section">
        <div class="btn-group season-dropdown float-right" role="group" aria-label="Basic example">
            <a href="/channels/all/all/newest.html" class="btn btn-secondary  active ">
                <i class="far fa-clock"></i>
                <span class="for-desktop">Newest</span>
            </a>
            <a href="/channels/all/all/views.html" class="btn btn-secondary ">
                <i class="far fa-eye"></i> 
                <span class="for-desktop">Views</span>
            </a>
            <a href="/channels/all/all/rating.html" class="btn btn-secondary ">
                <i class="fas fa-star-half-alt"></i> 
                <span class="for-desktop">Rating</span>
            </a>
            <a href="/channels/all/all/title.html" class="btn btn-secondary ">
                <i class="fas fa-sort-alpha-down"></i>
                <span class="for-desktop">Title</span>
            </a>
        </div>
        <div class="btn-group season-dropdown" role="group">
            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                All categories
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a class="dropdown-item" href="/channels/all/all/newest.html">All categories</a>
                <a class="dropdown-item" href="/channels/Sport/all/newest.html">Sport</a>
                <a class="dropdown-item" href="/channels/Animals/all/newest.html">Animals</a>
                <a class="dropdown-item" href="/channels/Entertainment/all/newest.html">Entertainment</a>
                <a class="dropdown-item" href="/channels/News/all/newest.html">News</a>
                <a class="dropdown-item" href="/channels/Music/all/newest.html">Music</a>
                <a class="dropdown-item" href="/channels/Family/all/newest.html">Family</a>
                <a class="dropdown-item" href="/channels/Documentary/all/newest.html">Documentary</a>
            </div>
        </div>
        <div class="btn-group season-dropdown" role="group">
            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                All Countries
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a class="dropdown-item" href="/channels/all/all/newest.html">All Countries</a>
                <a class="dropdown-item" href="/channels/all/USA/newest.html">
                    <img class="country-logo" src="https://webflix.virmana.com/uploads/cache/language_thumb_api/uploads/png/5d9358da49bd682b25d2ebf67df60952.png">
                    USA
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row content-section">
    <div></div>
    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>    
    <a class="channel col-4 col-md-2 col-xxl-2" href="<?php echo e(route('channel.slug', $item->slug)); ?>">
        <img height="75" width="150" src="<?php echo e($item->image); ?>" />
        <?php if($item->is_hd == 1): ?>
            <div>
                <span>HD</span>
                <p class="two">LIVE</p>
            </div>
        <?php endif; ?>
    </a>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>