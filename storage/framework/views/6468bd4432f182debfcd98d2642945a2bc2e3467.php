
<?php $__env->startSection('breadcrumb'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><i class="fa fa-file-audio-o"></i> Master Lagu</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Master Lagu</a>
                </li>
                <li class="active">
                    <strong>Table</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Master Lagu</h5>
                </div>
                <div class="ibox-content">
                    <?php if(auth()->user()->roles->first()->name == 'admin' ||
                        auth()->user()->roles->first()->name == 'legal' ||
                        auth()->user()->roles->first()->name == 'anr'): ?>
                        <form style="margin-bottom: 10px;" class="form form-inline" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <?php echo Form::select('singer', $filsingers, null, ['class' => 'input-sm form-control input-s-sm inline', 'id' => 'singer']); ?>

                                        </div>

                                    </div>
                                    <div class="col-md-2">
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-default clear-filtering"><i
                                                    class="fa fa-undo"></i> Clear</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Row -->
                            </div>
                        </form>
                    <?php endif; ?>
                    <?php echo $__env->make('backend.masterlagu.table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.backend.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>