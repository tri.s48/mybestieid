<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('css/fileinput.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/plugins/switchery/switchery.css')); ?>" rel="stylesheet">

    <style>
        .kv-photo .krajee-default.file-preview-frame,
        .kv-photo .krajee-default.file-preview-frame:hover {
            margin: 0;
            padding: 0;
            border: none;
            box-shadow: none;
            text-align: center;
        }

        .kv-photo .file-input {
            display: table-cell;
            max-width: 220px;
        }

        .kv-reqd {
            color: red;
            font-family: monospace;
            font-weight: normal;
        }

    </style>
    <?php echo $__env->make('layouts.backend.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<!-- Parent Field -->
<?php if($formType == 'edit'): ?>
    <div class="form-group">
        <?php echo Form::label('position', 'Position:', ['class' => 'col-sm-2 control-label']); ?>


        <div class="col-sm-10">
            <?php echo Form::select('position', ['0' => 'Top', '1' => 'Bottom'], null, ['placeholder' => 'Select Position', 'class' => 'form-control', 'readonly' => 'readonly']); ?>

        </div>
    </div>
<?php else: ?>
    <div class="form-group">
        <?php echo Form::label('position', 'Position:', ['class' => 'col-sm-2 control-label']); ?>


        <div class="col-sm-10">
            <?php echo Form::select('position', ['0' => 'Top', '1' => 'Bottom'], 1, ['placeholder' => 'Select Position', 'class' => 'form-control', 'readonly' => 'readonly']); ?>

        </div>
    </div>
<?php endif; ?>
<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('title', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 50]); ?>

    </div>
</div>

<div class="form-group">
    <?php echo Form::label('icon', 'Icon:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('icon', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 50]); ?>

    </div>
</div>

<div class="form-group">
    <?php echo Form::label('url', 'URL:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('url', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 70]); ?>

    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('is_active', 'Status:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <input type="hidden" name="is_active" value="0">
        <input type="checkbox" name="is_active" class="js-switch"
            <?php echo e(isset($menu) && $menu->is_active == 1 ? 'checked' : ''); ?> />
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
        <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

        <a href="<?php echo route('menu.index'); ?>" class="btn btn-default">Cancel</a>
    </div>
</div>

<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/piexif.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/sortable.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/purify.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/fileinput.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/tinymce/jquery.tinymce.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/tinymce/tinymce.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/switchery/switchery.js')); ?>"></script>
    <script>
        $(document).ready(function() {
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, {
                color: '#1AB394'
            });
        });

        var formType = '<?php echo e($formType); ?>';
    </script>
    <?php if($formType == 'edit'): ?>
        <?php echo $__env->make('layouts.backend.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <script>
            $.fn.dataTable.ext.buttons.create = {
                action: function(e, dt, button, config) {
                    $('#myModal').modal('show');
                }
            };

            var hash = document.location.hash;
            if (hash) {
                console.log(hash);
                $('.nav-tabs a[href="' + hash + '"]').tab('show')
            }

            // Change hash for page-reload
            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                window.location.hash = e.target.hash;
            });
        </script>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
