
<?php $__env->startSection('breadcrumb'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Master Singer</h2>
            <ol class="breadcrumb">
                <li>
                    <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                        <a href="<?php echo e(route('mastersinger.index')); ?>">Master Singer</a>
                    <?php elseif(auth()->user()->roles->first()->name === 'legal'): ?>
                        <a href="<?php echo e(route('legal.mastersinger.index')); ?>">Master Singer</a>
                    <?php endif; ?>
                </li>
                <li class="active">
                    <strong>Create</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentCrud'); ?>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Create Master Singer
                </div>
                <div class="panel-body">
                    <?php if(auth()->user()->roles->first()->name == 'admin'): ?>
                        <?php echo Form::open(['route' => ['mastersinger.store'], 'files' => true, 'class' => 'form-horizontal', 'id' => 'example-form']); ?>

                    <?php elseif(auth()->user()->roles->first()->name == 'legal'): ?>
                        <?php echo Form::open(['route' => ['legal.mastersinger.store'], 'files' => true, 'class' => 'form-horizontal', 'id' => 'example-form']); ?>

                    <?php elseif(auth()->user()->roles->first()->name == 'anr'): ?>
                        <?php echo Form::open(['route' => ['anr.mastersinger.store'], 'files' => true, 'class' => 'form-horizontal', 'id' => 'example-form']); ?>

                    <?php endif; ?>
                    <?php echo $__env->make('backend.masterlagu.singer.fields', ['formType' => 'create'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.backend.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>