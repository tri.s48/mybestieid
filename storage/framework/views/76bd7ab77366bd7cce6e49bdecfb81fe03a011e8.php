<?php if(auth()->user()->roles->first()->name == 'finance'): ?>
<div class='btn-group'>
    <?php if($status == 2 || $status == 3 || $status == 4 || $status == 5 || $status == 6): ?>
        <a href="<?php echo e(route('finance.revenue.view', $id)); ?>" class='btn btn-default btn-xs' title="Show Revenue">
            <i class="fa fa-eye"></i>
        </a>
    <?php endif; ?>
    <?php if($status == 2 || $status == 3 || $status == 4 || $status == 5 || $status == 6): ?>
        <a href="<?php echo e(route('finance.revenue.download.report', $id)); ?>" class='btn btn-default btn-xs' title="Download Revenue Excel">
            <i class="fa fa-file-excel-o"></i>
        </a>
    <?php endif; ?>
    <?php if($status == 2 || $status == 3 || $status == 4 || $status == 5 || $status == 6): ?>
        <a href="<?php echo e(route('finance.revenue.download.report_pdf', $id)); ?>" class='btn btn-default btn-xs' target="_blank" title="Print">
            <i class="fa fa-print"></i>
        </a>
    <?php endif; ?>

    
    <!-- <?php if(($status == 4 || $status == 5 || $status == 6)): ?>
    <a href="<?php echo e(route('finance.revenue.download', $id)); ?>" class='btn btn-default btn-xs' title="Download Revenue PDF">
        <i class="fa fa-file-pdf-o"></i>
    </a>
    <?php endif; ?>
    <?php if(($status == 4 || $status == 5 || $status == 6)): ?>
    <a href="<?php echo e(route('finance.revenue.excel', $id)); ?>" class='btn btn-default btn-xs' title="Download Revenue Excel">
        <i class="fa fa-file-excel-o"></i>
    </a>
    <?php endif; ?>
    <?php if($status == 1): ?>

    <a href="<?php echo e(route('finance.revenue.edit', $id)); ?>" data-url="<?php echo e(route('revenue.edit', $id)); ?>" class='btn btn-default btn-xs' title="Edit Revenue">
        <i class="fa fa-pencil"></i> Update Draft
    </a>
    <?php endif; ?> -->

    <?php if($status == 5 || $status == 6): ?>
    <a href="<?php echo e(route('finance.revenue.edit.revisi', $id)); ?>" class='btn btn-default btn-xs' title="Edit Revenue">
        <i class="fa fa-pencil"></i> Revisi
    </a>
    <?php endif; ?>
    <?php if($status == 4): ?>
        <a href="<?php echo e(route('finance.revenue.edit', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Status">
            <i class="fa fa-pencil"></i> Status Paid
        </a>
    <?php endif; ?>
    <?php if($status == 2 || $status == 6): ?>
        <a href="<?php echo e(route('finance.revenue.status_checked', $id)); ?>" class='btn btn-danger btn-xs' title="To Checked" onclick="return confirm('Are you sure to check?')">
            <i class="fa fa-check "></i> Checked
        </a>
    <?php endif; ?>
    <?php if($status == 3): ?>
    <a href="<?php echo e(route('finance.revenue.status_revision', $id)); ?>" class='btn btn-warning btn-xs'>
        <i class="fa fa-pencil"></i> Revision
    </a>
    <?php endif; ?>
    <?php if($status == 1 || $status == 2 || $status == 5 || $status == 6): ?>
    <a href="<?php echo e(route('finance.revenue.destroy_revenue', $id)); ?>" class='btn btn-warning btn-xs' title="Delete Revenue" onclick="return confirm('Are you sure to delete?')">
        <i class="fa fa-trash"></i> Delete
    </a>
    <?php endif; ?>
<?php else: ?>
    <div class='btn-group'>
        <?php if($status == 2 || $status == 3 || $status == 4 || $status == 5 || $status == 6): ?>
            <a href="<?php echo e(route('revenue.view', $id)); ?>" class='btn btn-default btn-xs' title="Show Revenue">
                <i class="fa fa-eye"></i>
            </a>
        <?php endif; ?>
        <?php if($status == 2 || $status == 3 || $status == 4 || $status == 5 || $status == 6): ?>
            <a href="<?php echo e(route('revenue.download.report', $id)); ?>" class='btn btn-default btn-xs' title="Download Revenue Excel">
                <i class="fa fa-file-excel-o"></i>
            </a>
        <?php endif; ?>
        <?php if($status == 2 || $status == 3 || $status == 4 || $status == 5 || $status == 6): ?>
            <a href="<?php echo e(route('revenue.download.report_pdf', $id)); ?>" class='btn btn-default btn-xs' target="_blank" title="Print">
                <i class="fa fa-print"></i>
            </a>
        <?php endif; ?>

        
        <!-- <?php if(($status == 4 || $status == 5 || $status == 6)): ?>
        <a href="<?php echo e(route('revenue.download', $id)); ?>" class='btn btn-default btn-xs' title="Download Revenue PDF">
            <i class="fa fa-file-pdf-o"></i>
        </a>
        <?php endif; ?>
        <?php if(($status == 4 || $status == 5 || $status == 6)): ?>
        <a href="<?php echo e(route('revenue.excel', $id)); ?>" class='btn btn-default btn-xs' title="Download Revenue Excel">
            <i class="fa fa-file-excel-o"></i>
        </a>
        <?php endif; ?> -->
        <!-- <?php if($status == 2 || $status == 3 || $status == 4 || $status == 5 || $status == 6): ?>

        <a href="<?php echo e(route('revenue.edit', $id)); ?>" data-url="<?php echo e(route('revenue.edit', $id)); ?>" class='btn btn-default btn-xs' title="Edit Revenue">
            <i class="fa fa-pencil"></i> Update Draft
        </a>
        <?php endif; ?> -->

        <?php if($status == 4): ?>
            <a href="<?php echo e(route('revenue.edit', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Status">
                <i class="fa fa-pencil"></i> Status Paid
            </a>
        <?php endif; ?>
        <?php if($status == 5 || $status == 6): ?>
        <a href="<?php echo e(route('revenue.edit.revisi', $id)); ?>" class='btn btn-default btn-xs' title="Edit Revenue">
            <i class="fa fa-pencil"></i> Revisi
        </a>
        <?php endif; ?>
        <?php if($status == 2 || $status == 6): ?>
            <a href="<?php echo e(route('revenue.status_checked', $id)); ?>" class='btn btn-danger btn-xs ' title="To Checked" onclick="return confirm('Are you sure to check?')">
                <i class="fa fa-check "></i> Checked
            </a>
        <?php endif; ?>
    </div>
<?php endif; ?>
