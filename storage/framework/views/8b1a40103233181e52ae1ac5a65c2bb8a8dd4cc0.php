

<?php $__env->startSection('content'); ?>
<div class="row content-section">
    <div class="col-md-12">
        <div class="movie-header-bg"
            style=" background-image: url('/uploads/png/b7004768d9fdea58d9c9f77bd4bb7fa5.png');">
            <div class="movie-header">
                <div class="movie-title"><?php echo e($data['title']); ?></div>
                <div class="movie-infos"> +12 • <a href="/channels/Sport/all/views.html">Sport</a> • <a
                        href="/channels/News/all/views.html">News</a></div>
                <div class="header-buttons">
                    <button class="open-login"> <i class="far fa-heart"></i></button>
                    <button id="share-btn">
                        <i class="fa fa-share "></i>SHARE
                        <div class="share-buttons">
                            <a
                                href="mailto:?Subject=VIR HD3&amp;Body=Son dakika spor haberleri, transfer haberleri, puan durumu, fikstür, canlı skor ve ASPOR Canlı yayın. https://webflix.virmana.com/c/share/38.html">
                                <i class="fas fa-envelope"></i>
                            </a>
                            <a href="http://www.facebook.com/sharer.php?u=https://webflix.virmana.com/c/share/38.html"
                                target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://webflix.virmana.com/c/share/38.html"
                                target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                            <a href="https://twitter.com/share?url=https://webflix.virmana.com/c/share/38.html&amp;text=VIR HD3"
                                target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                    </button>
                </div>
                <div class="header-ratings">
                    <a href="/channels/all/Turkey/views.html"> <img class="country-logo"
                            src="https://webflix.virmana.com/uploads/cache/language_thumb_api/uploads/png/2f5b26e6770471cf131bbcd0e741a7ec.png">
                    </a> • 4.1/5
                    <i class="fas fa-star checked"></i>
                    <i class="fas fa-star checked"></i>
                    <i class="fas fa-star checked"></i>
                    <i class="fas fa-star checked"></i>
                    <i class="far fa-star checked"></i>

                </div>
            </div>
            <img class="channel-logo" src="/uploads/png/b7004768d9fdea58d9c9f77bd4bb7fa5.png">
        </div>
    </div>
    <div class="col-md-12">
        <div class="dropdown">
            <a href="/player/c/38/vir-hd3/213.html" class="btn play-btn"><span class="fa fa-play"></span> Play
                Channel</a>
            <a href="https://www.aspor.com.tr/" target="_blank" id="trailer-btn" class="btn play-btn"><i
                    class="fas fa-globe-americas"></i> Visit website</a>
            <a class="btn play-btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <span class="fa fa-star"></span> Rate Channel
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <div class="comment-item  review-write">
                    <div class='alert alert-success success-review' role='alert'>
                        <i class='fas fa-comment-alt'></i> Your Review has been added successfully!
                    </div>
                    <div class='alert alert-danger error-review' role='alert'>
                        <i class='fas fa-comment-alt'></i> Your Review could not be submitted
                    </div>
                    <div class="rate" id="rate-input" alt="38">
                        <input type="radio" id="star5" name="rate" value="5" />
                        <label for="star5" title="text">5 stars</label>
                        <input type="radio" id="star4" name="rate" value="4" />
                        <label for="star4" title="text">4 stars</label>
                        <input type="radio" id="star3" name="rate" value="3" />
                        <label for="star3" title="text">3 stars</label>
                        <input type="radio" id="star2" name="rate" value="2" />
                        <label for="star2" title="text">2 stars</label>
                        <input type="radio" checked="true" id="star1" name="rate" value="1" />
                        <label for="star1" title="text">1 star</label>
                    </div>
                    <textarea class="input-review" placeholder="Write you review here"></textarea>
                    <button class="btn btn-primary btn-block open-login"> <i class="fas fa-check"></i> Login to leave
                        a review </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 padding-top-20">
        <div class="movie-section-title">Overview</div>
    </div>
    <div class="col-md-12">
        <p><?php echo $data['content']; ?></p>
    </div>
    <div class="col-md-12 padding-top-20">
        <div class="movie-section-title">For You</div>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            <?php $__currentLoopData = $newOtherRecomendation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recomendation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>    
            <a class="channel" href="<?php echo e(route('channel.slug', $recomendation['slug'])); ?>">
                <img src="<?php echo e($recomendation['image']); ?>" />
            </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <div class="col-md-6 padding-top-20">
        <div class="movie-section-title"><i class="fa fa-comment"></i> 4 Comments</div>
        <div class="comment-section comment-list">
            <div class="comment-item">
                <a title="View profile" href="#" class="avatar-thumb"><img
                        src="https://lh3.googleusercontent.com/a-/AOh14GjTGDSwqtoIf8QJTUva_YfePWonxv2lpYKxZYlc4Q"></a>
                <div class="comment-text">
                    <a>ARTHUR DE JESUS</a><span class="float-right"><i class="fa fa-clock-o"></i> 1 year ago </span>
                    <p> cool app</p>
                </div>
            </div>
            <div class="comment-item">
                <a title="View profile" href="#" class="avatar-thumb"><img
                        src="https://webflix.virmana.com/uploads/cache/actor_thumb_mini_web/uploads/png/4b511878081fe6299834505f94d65dd6.png"></a>
                <div class="comment-text">
                    <a>Demo</a><span class="float-right"><i class="fa fa-clock-o"></i> 1 year ago </span>
                    <p> jgkgklkfldjj kjkfjg</p>
                </div>
            </div>
            <div class="comment-item">
                <a title="View profile" href="#" class="avatar-thumb"><img
                        src="https://lh3.googleusercontent.com/a-/AOh14Gg-KNp5207QzosPvMWTjPZ1n20-nrAWfyYxq66oVA=s96-c"></a>
                <div class="comment-text">
                    <a>Hassan Boutannoura</a><span class="float-right"><i class="fa fa-clock-o"></i> 2 months ago
                    </span>
                    <p> wwwwww</p>
                </div>
            </div>
            <div class="comment-item">
                <a title="View profile" href="#" class="avatar-thumb"><img
                        src="https://lh3.googleusercontent.com/a-/AOh14GiTIkgN98Rmez-d-rk4T1xeecXbFh97OtoyYxUg"></a>
                <div class="comment-text">
                    <a>Matthew chief</a><span class="float-right"><i class="fa fa-clock-o"></i> 1 month ago </span>
                    <p> xxd</p>
                </div>
            </div>
        </div>
        <div class='alert alert-success success-comment' role='alert'>
            <i class='fas fa-comment-alt'></i> Your comment has been added successfully!
        </div>
        <div class='alert alert-danger error-comment' role='alert'>
            <i class='fas fa-comment-alt'></i> Your comment could not be submitted
        </div>
        <button class="btn comment-btn-item open-login">
            <i class="fa fa-check"></i> Login to leave a comment
        </button>
    </div>
    <div class="col-md-6 padding-top-20">
        <div class="movie-section-title"><i class="fa fa-star"></i> 9 Reviews</div>
        <div class="comment-section ">
            <div class="review">
                <div class="review-properties">
                    Reviewed by
                    <span class="review-author">Hassan Boutannoura</span>
                    <span class="fa fa-star checked"></span>
                    <span class="review-rating">4 / 5</span>
                    <span class="float-right"><i class="fa fa-clock-o"></i> 2 months ago </span>
                </div>
                <article>
                    <p></p>
                </article>
            </div>
            <div class="review">
                <div class="review-properties">
                    Reviewed by
                    <span class="review-author">adilsonfelixda Silva</span>
                    <span class="fa fa-star checked"></span>
                    <span class="review-rating">0 / 5</span>
                    <span class="float-right"><i class="fa fa-clock-o"></i> 1 year ago </span>
                </div>
                <article>
                    <p></p>
                </article>
            </div>
            <div class="review">
                <div class="review-properties">
                    Reviewed by
                    <span class="review-author">Asad Khan</span>
                    <span class="fa fa-star checked"></span>
                    <span class="review-rating">5 / 5</span>
                    <span class="float-right"><i class="fa fa-clock-o"></i> 1 year ago </span>
                </div>
                <article>
                    <p></p>
                </article>
            </div>
            <div class="review">
                <div class="review-properties">
                    Reviewed by
                    <span class="review-author">Trap music Fan</span>
                    <span class="fa fa-star checked"></span>
                    <span class="review-rating">5 / 5</span>
                    <span class="float-right"><i class="fa fa-clock-o"></i> 1 year ago </span>
                </div>
                <article>
                    <p></p>
                </article>
            </div>
            <div class="review">
                <div class="review-properties">
                    Reviewed by
                    <span class="review-author">Cimbom ON</span>
                    <span class="fa fa-star checked"></span>
                    <span class="review-rating">4 / 5</span>
                    <span class="float-right"><i class="fa fa-clock-o"></i> 1 year ago </span>
                </div>
                <article>
                    <p></p>
                </article>
            </div>
            <div class="review">
                <div class="review-properties">
                    Reviewed by
                    <span class="review-author">Mehmet Uzun</span>
                    <span class="fa fa-star checked"></span>
                    <span class="review-rating">4 / 5</span>
                    <span class="float-right"><i class="fa fa-clock-o"></i> 1 year ago </span>
                </div>
                <article>
                    <p></p>
                </article>
            </div>
            <div class="review">
                <div class="review-properties">
                    Reviewed by
                    <span class="review-author">Onur Öztaş</span>
                    <span class="fa fa-star checked"></span>
                    <span class="review-rating">5 / 5</span>
                    <span class="float-right"><i class="fa fa-clock-o"></i> 2 years ago </span>
                </div>
                <article>
                    <p></p>
                </article>
            </div>
            <div class="review">
                <div class="review-properties">
                    Reviewed by
                    <span class="review-author">Ottoman Grandson</span>
                    <span class="fa fa-star checked"></span>
                    <span class="review-rating">5 / 5</span>
                    <span class="float-right"><i class="fa fa-clock-o"></i> 2 years ago </span>
                </div>
                <article>
                    <p></p>
                </article>
            </div>
            <div class="review">
                <div class="review-properties">
                    Reviewed by
                    <span class="review-author">Onur Öztaş</span>
                    <span class="fa fa-star checked"></span>
                    <span class="review-rating">5 / 5</span>
                    <span class="float-right"><i class="fa fa-clock-o"></i> 2 years ago </span>
                </div>
                <article>
                    <p></p>
                </article>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>