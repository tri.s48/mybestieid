<?php if(auth()->user()->roles->first()->name == 'admin'): ?>
    <?php echo Form::open(['route' => ['creator.destroy', $c->id], 'method' => 'delete']); ?>

    <div class='btn-group'>
        <a href="<?php echo e(route('influencer.edit', $c->id)); ?>" class='btn btn-info btn-xs' title="Add To Influencer">
            <i class="glyphicon glyphicon-star"></i>
        </a>
        <a href="<?php echo e(route('creator.edit', $c->id)); ?>" class='btn btn-primary btn-xs' title="Edit Creator">
            <i class="glyphicon glyphicon-edit"></i>
        </a>
        <?php echo Form::button('<i class="glyphicon glyphicon-remove"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-warning btn-xs',
            'title' => 'Delete Creator',
            'onclick' => "return confirm('Do you want to delete this creator?')",
            'name' => 'action',
            'value' => 'del'
        ]); ?>

        <?php if($c->is_active == 1): ?>
        <?php echo Form::button('Inactive', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Inactive Creator',
            'onclick' => "return confirm('Do you want to inactive this creator?')",
            'name' => 'action',
            'value' => 'inact'
        ]); ?>

        <?php else: ?>
        <?php echo Form::button('Active', [
            'type' => 'submit',
            'class' => 'btn btn-success btn-xs',
            'title' => 'Activated Creator',
            'onclick' => "return confirm('Do you want to activated this creator?')",
            'name' => 'action',
            'value' => 'act'
        ]); ?>

        <?php endif; ?>
        <?php if($c->is_show == 1): ?>
        <?php echo Form::button('Hide', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Inactive Creator',
            'onclick' => "return confirm('Do you want to hide this creator?')",
            'name' => 'action',
            'value' => 'hideact'
        ]); ?>

        <?php else: ?>
        <?php echo Form::button('Show', [
            'type' => 'submit',
            'class' => 'btn btn-success btn-xs',
            'title' => 'Activated Creator',
            'onclick' => "return confirm('Do you want to show this creator?')",
            'name' => 'action',
            'value' => 'showact'
        ]); ?>

        <?php endif; ?>
    </div>
    <?php echo Form::close(); ?>

<?php else: ?>
    <?php echo Form::open(['route' => ['creator.destroy', $c->id], 'method' => 'delete']); ?>

    <div class='btn-group'>
        <a href="<?php echo e(route('influencer.edit', $c->id)); ?>" class='btn btn-default btn-xs' title="Add To Influencer">
            <i class="glyphicon glyphicon-star"></i>
        </a>
        <a href="<?php echo e(route('creator.edit', $c->id)); ?>" class='btn btn-default btn-xs' title="Edit Creator">
            <i class="glyphicon glyphicon-edit"></i>
        </a>
        <?php if($c->is_active == 1): ?>
        <?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Inactive Creator',
            'onclick' => "return confirm('Do you want to inactive this creator?')",
            'name' => 'action',
            'value' => 'inact'
        ]); ?>

        <?php else: ?>
        <?php echo Form::button('<i class="glyphicon glyphicon-ok"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-success btn-xs',
            'title' => 'Activated Creator',
            'onclick' => "return confirm('Do you want to activated this creator?')",
            'name' => 'action',
            'value' => 'act'
        ]); ?>

        <?php endif; ?>
        
    </div>
    <?php echo Form::close(); ?>

<?php endif; ?>