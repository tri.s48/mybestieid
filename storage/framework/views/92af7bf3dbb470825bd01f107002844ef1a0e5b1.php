<?php $__env->startSection('css'); ?>
    <?php echo $__env->make('layouts.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $dataTable->table(['width' => '100%']); ?>


<?php $__env->startSection('scripts'); ?>
    <?php echo $__env->make('layouts.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $dataTable->scripts(); ?>

    <?php echo $__env->make('layouts.datatables_limit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script>
    function ConfirmDelete()
    {
    var x = confirm("Are you sure you want to delete?");
    if (x)
        return true;
    else
        return false;
    }
    $('.form-horizontal').find('.btn-search').on('click', function(e) {
        LaravelDataTables["dataTableBuilder"].draw();
        e.preventDefault();
    });
    $('.form-horizontal').find('.clear-filtering').on('click', function(e) {
        $('#month').val('');
        LaravelDataTables["dataTableBuilder"].draw();
        e.preventDefault();
    });

    LaravelDataTables["dataTableBuilder"].on('click', '.edit-revision', function(e){
        e.preventDefault();
        var url = $(this).data('url');
        $("#form-revision").attr('action', url);
        $('#modal-revision').modal('show');
        $('#btn-upload').click(function(e) {
            $('#edit-status').val('csv');
        });
    });

    </script>
<?php $__env->stopSection(); ?>