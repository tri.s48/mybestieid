<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="logo" style="margin:-67px -12px;">                
                    <img alt="MyBestieID" width="100%" src="<?php echo e(asset('frontend/logo-home.png')); ?>">
                </div>
                
            </li>
            <!-- ++++++++++++++  ROLE ADMINISTRATOR  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <?php if(auth()->user()->hasRole(['admin'])): ?>
            <li class="<?php echo e(Request::is('admin/dashboard*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.dashboard.index')); ?>"><i class="fa fa-dashboard <?php echo e((Request::is('dashboard') ? 'active' : '')); ?>"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/channel*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('channel.index')); ?>"><i class="fa fa-adjust <?php echo e((Request::is('channel') ? 'active' : '')); ?>"></i> <span class="nav-label">Channel</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/creator*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.index')); ?>"><i class="fa fa-group <?php echo e((Request::is('creator') ? 'active' : '')); ?>"></i> <span class="nav-label">Creator</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/serie*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.serie.index')); ?>"><i class="fa fa-star <?php echo e((Request::is('serie') ? 'active' : '')); ?>"></i> <span class="nav-label">Series</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/posts*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.posts.index')); ?>"><i class="fa fa-play <?php echo e((Request::is('posts') ? 'active' : '')); ?>"></i> <span class="nav-label">Posts</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/ads*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('ads.index')); ?>"><i class="fa fa-language <?php echo e((Request::is('ads') ? 'active' : '')); ?>"></i> <span class="nav-label">Ads</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/brand*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('brand.index')); ?>"><i class="fa fa-superpowers <?php echo e((Request::is('brand') ? 'active' : '')); ?>"></i> <span class="nav-label">Brands</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/influencer*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-instagram"></i> <span class="nav-label">Influencers</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('admin/influencer') ? 'active' : ''); ?>"><a href="<?php echo e(route('influencer.index')); ?>"><i class="fa fa fa-diamond"></i> Influencers</a></li>
                    <li class="<?php echo e(Request::is('admin/influencer/browse-influencer*') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.influencer.browse')); ?>"><i class="fa fa-users"></i> Browse Influencer</a></li>
                </ul>
            </li>
            <!-- <li class="<?php echo e(Request::is('admin/influencer*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('influencer.index')); ?>"><i class="fa fa-instagram <?php echo e((Request::is('influencer') ? 'active' : '')); ?>"></i> <span class="nav-label">Influencers</span></a>
            </li> -->
            <li class="<?php echo e(Request::is('admin/revenue*') ? 'active' : ''); ?><?php echo e(Request::is('admin/dsp*') ? 'active' : ''); ?><?php echo e(Request::is('admin/youtube/analytics*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">Reports</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('admin/revenue*') ? 'active' : ''); ?> <?php echo e(Request::is('admin/youtube/analytics*') ? 'active' : ''); ?>">
                        <a href="#"><i class="fa fa-youtube"></i> <span class="nav-label">Youtube</span>
                        <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li class="<?php echo e(Request::is('admin/revenue') ? 'active' : ''); ?><?php echo e(Request::is('admin/revenue/*/*') ? 'active' : ''); ?>"><a href="<?php echo e(route('revenue.index')); ?>"><i class="fa fa-file-text"></i> Revenue</a></li>
                            <li class="<?php echo e(Request::is('admin/revenue/approval*') ? 'active' : ''); ?>"><a href="<?php echo e(route('revenue.approval.index')); ?>"><i class="fa fa-check-square-o"></i> Pending Approval</a></li>
                            <li class="<?php echo e(Request::is('admin/revenue/approver*') ? 'active' : ''); ?>"><a href="<?php echo e(route('revenue.approver.index')); ?>"><i class="fa fa-user-plus"></i> Approver</a></li>
                            <li class="<?php echo e(Request::is('admin/youtube/analytics*') ? 'active' : ''); ?>"><a href="<?php echo e(route('youtube.analytics.index')); ?>"><i class="fa fa-bar-chart"></i> Analytics</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo e(Request::is('admin/dsp*') ? 'active' : ''); ?>"><a href="<?php echo e(route('dsp.index')); ?>"><i class="fa fa-file-audio-o"></i> DSP Reports</a></li>
                </ul>
            </li>
            <li class="<?php echo e(Request::is('admin/campaign*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.campaign.index')); ?>"><i class="fa fa-handshake-o <?php echo e((Request::is('campaign') ? 'active' : '')); ?>"></i> <span class="nav-label">Campaign</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/masterlagu*') ? 'active' : ''); ?><?php echo e((Request::is('admin/mastersinger*') ? 'active' : '')); ?><?php echo e((Request::is('admin/mastersongwriter*') ? 'active' : '')); ?>">
                <a href="#"><i class="fa fa-music"></i> <span class="nav-label">Master Data</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('admin/masterlagu*') ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('masterlagu.index')); ?>"><i class="fa fa-music <?php echo e((Request::is('admin/masterlagu') ? 'active' : '')); ?>"></i> <span class="nav-label">Master Lagu</span></a>
                    </li>
                    <li class="<?php echo e(Request::is('admin/mastersinger*') ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('mastersinger.index')); ?>"><i class="fa fa-star <?php echo e((Request::is('admin/mastersinger') ? 'active' : '')); ?>"></i> <span class="nav-label">Master Singer</span></a>
                    </li>
                    <li class="<?php echo e(Request::is('admin/mastersongwriter*') ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('mastersongwriter.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('admin/mastersongwriter') ? 'active' : '')); ?>"></i> <span class="nav-label">Master Songwriter</span></a>
                    </li>
                </ul>
            </li>
            <!-- <li class="<?php echo e(Request::is('admin/platforms*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('platforms.index')); ?>"><i class="<?php echo e((Request::is('platforms') ? 'active' : '')); ?>"></i> <span class="nav-label">Platforms</span></a>
            </li> -->
            <li class="<?php echo e(Request::is('admin/config*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('config.index')); ?>"><i class="fa fa-gears <?php echo e((Request::is('config') ? 'active' : '')); ?>"></i> <span class="nav-label">Website Config</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/email_config*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('email_config.index')); ?>"><i class="fa fa-envelope <?php echo e((Request::is('email_config') ? 'active' : '')); ?>"></i> <span class="nav-label">Email Config</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/personal*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.personal.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('personal') ? 'active' : '')); ?>"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            <!-- <li class="<?php echo e(Request::is('admin/payment*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('payment.index')); ?>"><i class="<?php echo e((Request::is('payment') ? 'active' : '')); ?>"></i> <span class="nav-label">Payment Setting</span></a>
            </li> -->
            <li class="<?php echo e(Request::is('admin/page*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('page.index')); ?>"><i class="fa fa-file <?php echo e((Request::is('page') ? 'active' : '')); ?>"></i> <span class="nav-label">Pages</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/menu*', 'admin/user*', 'admin/role*', 'admin/category*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">System Admin</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('admin/user*') ? 'active' : ''); ?>"><a href="<?php echo e(route('user.index')); ?>"><i class="fa fa-user-plus"></i> User Management</a></li>
                    <!--<li class="<?php echo e(Request::is('admin/menu*') ? 'active' : ''); ?>"><a href="<?php echo e(route('role.index')); ?>">Menu Management</a></li>-->
                    <li class="<?php echo e(Request::is('admin/role*') ? 'active' : ''); ?>"><a href="<?php echo e(route('role.index')); ?>"><i class="fa fa-users"></i> Role Management</a></li>
                    <li class="<?php echo e(Request::is('admin/category*') ? 'active' : ''); ?>"><a href="<?php echo e(route('category.index')); ?>"><i class="fa fa-clone"></i> Product Category</a></li>
                    <li class="<?php echo e(Request::is('admin/menu*') ? 'active' : ''); ?>"><a href="<?php echo e(route('menu.index')); ?>"><i class="fa fa-navicon"></i> Menu Management</a></li>
                    <li class="<?php echo e(Request::is('admin/account_management*') ? 'active' : ''); ?>"><a href="<?php echo e(route('account_management.index')); ?>"><i class="fa fa-navicon"></i> Akun Management</a></li>
                </ul>
            </li>
            <?php elseif(auth()->user()->hasRole(['user'])): ?>
            <li class="<?php echo e(Request::is('user/dashboard*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('user.dashboard.index')); ?>"><i class="fa fa-dashboard <?php echo e((Request::is('dashboard') ? 'active' : '')); ?>"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            
            
            <li class="<?php echo e(Request::is('user/cms*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-globe"></i> <span class="nav-label">Network</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('user/cms') ? 'active' : ''); ?>"><a href="<?php echo e(route('user.cms.index')); ?>"><i class="fa fa-list"></i> CMS List</a></li>
                </ul>
            </li>
            <li class="<?php echo e(Request::is('user/creator*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-star-o"></i> <span class="nav-label">Channels</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('user/channels/leaderboard') ? 'active' : ''); ?>"><a href="<?php echo e(route('user.channels.leaderboard.index')); ?>"><i class="fa fa-trophy"></i> Leaderboard</a></li>
                    <li class="<?php echo e(Request::is('user/channels/list') ? 'active' : ''); ?>"><a href="<?php echo e(route('user.channels.list.index')); ?>"><i class="fa fa-star"></i>Channels List</a></li>
                </ul>
            </li>
            <li class="<?php echo e(Request::is('user/personal*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('user.personal.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('personal') ? 'active' : '')); ?>"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            <?php elseif(auth()->user()->hasRole('creator') && request()->segment(1) == 'creator'): ?>
            
            <li class="<?php echo e(Request::is('creator/serie*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.serie.index')); ?>"><i class="fa fa-star <?php echo e((Request::is('serie') ? 'active' : '')); ?>"></i> <span class="nav-label">Series</span></a>
            </li>
            <li class="<?php echo e(Request::is('creator/posts*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.posts.index')); ?>"><i class="fa fa-play <?php echo e((Request::is('posts') ? 'active' : '')); ?>"></i> <span class="nav-label">Posts</span></a>
            <li class="<?php echo e(Request::is('creator/bookmark*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.bookmark.index')); ?>"><i class="fa fa-address-book <?php echo e((Request::is('bookmark') ? 'active' : '')); ?>"></i> <span class="nav-label">Bookmark</span></a>
            </li>
            <li class="<?php echo e(Request::is('creator/platforms*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.platforms.index')); ?>"><i class="fa fa-globe <?php echo e((Request::is('platforms') ? 'active' : '')); ?>"></i> <span class="nav-label">Platforms</span></a>
            </li>
            <li class="<?php echo e(Request::is('creator/multiUpload*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.multiUpload.getUpload')); ?>"><i class="fa fa-upload <?php echo e((Request::is('multiUpload') ? 'active' : '')); ?>"></i> <span class="nav-label">Multi Upload</span></a>
            </li>
            <li class="<?php echo e(Request::is('creator/analytic*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.analytic.index')); ?>"><i class="fa fa-bar-chart <?php echo e((Request::is('analytic') ? 'active' : '')); ?>"></i> <span class="nav-label">Analytic</span></a>
            </li>
            
            <li class="<?php echo e(Request::is('creator/revenue*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.revenue.index')); ?>"><i class="fa fa-money <?php echo e((Request::is('revenue') ? 'active' : '')); ?>"></i> <span class="nav-label">Revenue</span></a>
            </li>
            
            <li class="<?php echo e(Request::is('creator/personal') ? 'active' : ''); ?><?php echo e(Request::is('creator/setting*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Setting</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('creator/personal') ? 'active' : ''); ?>"><a href="<?php echo e(route('creator.personal.index')); ?>"><i class=""></i> Personal</a></li>
                    
                    <li class="<?php echo e(Request::is('creator/setting/*/payment*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.creator.edit', [auth()->id(), 'payment'])); ?>"><i class=""></i> Payment</a></li>
                    <li class="<?php echo e(Request::is('creator/setting/*/rate_card*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.creator.edit', [auth()->id(), 'rate_card'])); ?>"><i class=""></i> Rate Card</a></li>
                    <li class="<?php echo e(Request::is('creator/setting/*/social_media*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.creator.edit', [auth()->id(), 'social_media'])); ?>"><i class=""></i> Social Media</a></li>
                </ul>
            </li>
            <?php elseif(auth()->user()->roles->first()->name === 'brand'): ?>
            <li class="<?php echo e(Request::is('brand/influencer*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('brand.influencer.browse')); ?>"><i class="fa fa-users <?php echo e((Request::is('influencer') ? 'active' : '')); ?>"></i> <span class="nav-label">Browse Influencer</span></a>
            </li>
            <li class="<?php echo e(Request::is('brand/campaign*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('brand.campaign.index')); ?>"><i class="fa fa-handshake-o <?php echo e((Request::is('campaign') ? 'active' : '')); ?>"></i> <span class="nav-label">Campaign</span></a>
            </li>
            <li class="<?php echo e(Request::is('brand/personal*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('brand.personal.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('personal') ? 'active' : '')); ?>"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            <?php elseif(auth()->user()->hasRole('influencer') && request()->segment(1) == 'influencer'): ?>
            <li class="<?php echo e(Request::is('influencer/campaign*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('influencer.campaign.index')); ?>"><i class="fa fa-handshake-o <?php echo e((Request::is('campaign') ? 'active' : '')); ?>"></i> <span class="nav-label">Campaign</span></a>
            </li>
            <li class="<?php echo e(Request::is('influencer/setting*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Setting</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('influencer/setting/*/personal*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.influencer.edit', [auth()->id(), 'personal'])); ?>"><i class=""></i> Personal</a></li>
                    <li class="<?php echo e(Request::is('influencer/setting/*/payment*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.influencer.edit', [auth()->id(), 'payment'])); ?>"><i class=""></i> Payment</a></li>
                    <li class="<?php echo e(Request::is('influencer/setting/*/rate_card*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.influencer.edit', [auth()->id(), 'rate_card'])); ?>"><i class=""></i> Rate Card</a></li>
                    <li class="<?php echo e(Request::is('influencer/setting/*/social_media*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.influencer.edit', [auth()->id(), 'social_media'])); ?>"><i class=""></i> Social Media</a></li>
                </ul>
            </li>
            <!-- ++++++++++++++  END ROLE ADMINISTRATOR  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE MEMBER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <?php elseif(auth()->user()->hasRole('member')): ?>
            <li class="<?php echo e(Request::is('member*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Setting</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('member/*/edit*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.member.edit', [auth()->id()])); ?>"><i class=""></i> Personal</a></li>
                    <li class="<?php echo e(Request::is('member/*/upgrade*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.member.upgrade', [auth()->id()])); ?>"><i class=""></i> Upgrade Account</a></li>
                </ul>
            </li>
            <!-- ++++++++++++++  END ROLE MEMBER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE FINANCE  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <?php elseif(auth()->user()->hasRole('finance')): ?>
            <li class="<?php echo e(Request::is('finance/dashboard*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('finance.dashboard.index')); ?>"><i class="fa fa-dashboard <?php echo e((Request::is('dashboard') ? 'active' : '')); ?>"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <!-- <li class="<?php echo e(Request::is('finance/creator*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('finance.creator.index')); ?>"><i class="fa fa-group <?php echo e((Request::is('creator') ? 'active' : '')); ?>"></i> <span class="nav-label">Creator</span></a>
            </li> -->
            <li class="<?php echo e(Request::is('finance/revenue*') ? 'active' : ''); ?><?php echo e(Request::is('finance/dsp*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">Reports</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('finance/revenue') ? 'active' : ''); ?><?php echo e(Request::is('finance/revenue/approval*') ? 'active' : ''); ?><?php echo e(Request::is('finance/revenue/approver*') ? 'active' : ''); ?>">
                        <a href="#"><i class="fa fa-youtube"></i> <span class="nav-label">Youtube</span>
                        <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li class="<?php echo e(Request::is('finance/revenue') ? 'active' : ''); ?>"><a href="<?php echo e(route('finance.revenue.index')); ?>"><i class="fa fa-file-text"></i> Revenue</a></li>
                            
                            <li class="<?php echo e(Request::is('finance/revenue/approval*') ? 'active' : ''); ?>"><a href="<?php echo e(route('finance.revenue.approval.index')); ?>"><i class="fa fa-check-square-o"></i> Pending Approval</a></li>
                            
                        </ul>
                    </li>
                    <li class="<?php echo e(Request::is('finance/dsp*') ? 'active' : ''); ?>"><a href="<?php echo e(route('finance.dsp.index')); ?>"><i class="fa fa-file-audio-o"></i> DSP Reports</a></li>
                </ul>
            </li>
            <li class="<?php echo e(Request::is('finance/personal*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('finance.personal.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('personal') ? 'active' : '')); ?>"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            

            <!-- ++++++++++++++  END ROLE FINANCE  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE LEGAL  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <?php elseif(auth()->user()->hasRole('legal')): ?>
            <li class="<?php echo e(Request::is('legal/dashboard*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('legal.dashboard.index')); ?>"><i class="fa fa-dashboard <?php echo e((Request::is('dashboard') ? 'active' : '')); ?>"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="<?php echo e((Request::is('legal/masterlagu*') ? 'active' : '')); ?><?php echo e((Request::is('legal/mastersinger*') ? 'active' : '')); ?><?php echo e((Request::is('legal/mastersongwriter*') ? 'active' : '')); ?>">
                <a href="#"><i class="fa fa-music"></i> <span class="nav-label">Master Data</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('legal/masterlagu*') ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('legal.masterlagu.index')); ?>"><i class="fa fa-music <?php echo e((Request::is('legal/masterlagu') ? 'active' : '')); ?>"></i> <span class="nav-label">Master Lagu</span></a>
                    </li>
                    <li class="<?php echo e(Request::is('legal/mastersinger*') ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('legal.mastersinger.index')); ?>"><i class="fa fa-star <?php echo e((Request::is('legal/mastersinger') ? 'active' : '')); ?>"></i> <span class="nav-label">Singer</span></a>
                    </li>
                    <li class="<?php echo e(Request::is('legal/mastersongwriter*') ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('legal.mastersongwriter.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('legal/mastersongwriter') ? 'active' : '')); ?>"></i> <span class="nav-label">Songwriter/Publisher</span></a>
                    </li>
                </ul>
            </li>
            <li class="<?php echo e(Request::is('legal/personal*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('legal.personal.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('personal') ? 'active' : '')); ?>"></i> <span class="nav-label">Personal Setting</span></a>
            </li>

            <!-- ++++++++++++++  END ROLE LEGAL  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE SINGER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <?php elseif(auth()->user()->hasRole('singer')): ?>
            <li class="<?php echo e(Request::is('singer/dashboard*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('singer.dashboard.index')); ?>"><i class="fa fa-dashboard <?php echo e((Request::is('dashboard') ? 'active' : '')); ?>"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="<?php echo e(Request::is('singer/masterlagu*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('singer.masterlagu.index')); ?>"><i class="fa fa-music <?php echo e((Request::is('masterlagu') ? 'active' : '')); ?>"></i> <span class="nav-label">Master Lagu</span></a>
            </li>
            <li class="<?php echo e(Request::is('singer/dsp*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('singer.dsp.index')); ?>"><i class="fa fa-music <?php echo e((Request::is('dsp') ? 'active' : '')); ?>"></i> <span class="nav-label">DSP Reports</span></a>
            </li>
            <li class="<?php echo e(Request::is('singer/personal*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('singer.personal.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('personal') ? 'active' : '')); ?>"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            <!-- ++++++++++++++  END ROLE SINGER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE SONGWRITER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <?php elseif(auth()->user()->hasRole('songwriter')): ?>
            <li class="<?php echo e(Request::is('songwriter/dashboard*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('songwriter.dashboard.index')); ?>"><i class="fa fa-dashboard <?php echo e((Request::is('dashboard') ? 'active' : '')); ?>"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="<?php echo e(Request::is('songwriter/masterlagu*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('songwriter.masterlagu.index')); ?>"><i class="fa fa-music <?php echo e((Request::is('masterlagu') ? 'active' : '')); ?>"></i> <span class="nav-label">Master Lagu</span></a>
            </li>
            <li class="<?php echo e(Request::is('songwriter/dsp*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('songwriter.dsp.index')); ?>"><i class="fa fa-music <?php echo e((Request::is('dsp') ? 'active' : '')); ?>"></i> <span class="nav-label">DSP Reports</span></a>
            </li>
            <li class="<?php echo e(Request::is('songwriter/personal*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('songwriter.personal.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('personal') ? 'active' : '')); ?>"></i> <span class="nav-label">Personal Setting</span></a>
            </li>

            <!-- ++++++++++++++  END ROLE SONGWRITER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            
            <!-- ++++++++++++++  ROLE ANR  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <?php elseif(auth()->user()->hasRole('anr')): ?>
            <li class="<?php echo e(Request::is('anr/dashboard*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('anr.dashboard.index')); ?>"><i class="fa fa-dashboard <?php echo e((Request::is('dashboard') ? 'active' : '')); ?>"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="<?php echo e((Request::is('anr/masterlagu*') ? 'active' : '')); ?><?php echo e((Request::is('anr/mastersinger*') ? 'active' : '')); ?><?php echo e((Request::is('anr/mastersongwriter*') ? 'active' : '')); ?>">
                <a href="#"><i class="fa fa-music"></i> <span class="nav-label">Master Data</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('anr/masterlagu*') ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('anr.masterlagu.index')); ?>"><i class="fa fa-music <?php echo e((Request::is('anr/masterlagu') ? 'active' : '')); ?>"></i> <span class="nav-label">Master Lagu</span></a>
                    </li>
                    <li class="<?php echo e(Request::is('anr/mastersinger*') ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('anr.mastersinger.index')); ?>"><i class="fa fa-star <?php echo e((Request::is('anr/mastersinger') ? 'active' : '')); ?>"></i> <span class="nav-label">Singer</span></a>
                    </li>
                    <li class="<?php echo e(Request::is('anr/mastersongwriter*') ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('anr.mastersongwriter.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('anr/mastersongwriter') ? 'active' : '')); ?>"></i> <span class="nav-label">Songwriter/Publisher</span></a>
                    </li>
                </ul>
            </li>
            
            <li class="<?php echo e(Request::is('anr/personal*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('anr.personal.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('personal') ? 'active' : '')); ?>"></i> <span class="nav-label">Personal Setting</span></a>
            </li>

            <!-- ++++++++++++++  END ROLE ANR  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE CMS  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <?php elseif(auth()->user()->hasRole('cms')): ?>
            <li class="<?php echo e(Request::is('cms/dashboard*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('cms.dashboard.index')); ?>"><i class="fa fa-dashboard <?php echo e((Request::is('dashboard') ? 'active' : '')); ?>"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            
            
            

            <!-- ++++++++++++++  END ROLE CMS  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            <?php endif; ?>
        </ul>
    </div>
</nav>