<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="images/favicon.png" />
    <title>Bestie - Free Movies / Tv Series / Tv Channels</title>
    <meta property="description" content="">
    <meta property="keywords" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:site_name" content=" ">
    <meta property="og:image" content="">
    <!-- Bootstrap -->
    <meta property="og:url" content="/" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="<?php echo e(asset('frontend/css/style.css')); ?>" rel="stylesheet">
    <script src="https://kit.fontawesome.com/017eaf7e3f.js" crossorigin="anonymous"></script>
</head>

<body class="dark-mode">
    <div class="container-fluid">
        <div class="row main-row">
            <div class="sidebar">
                <?php echo $__env->make('layouts.frontend.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="close-menu-left-btn"></div>
            <div class="main">
                <?php echo $__env->make('layouts.frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="content">
                    <?php echo $__env->yieldContent('content'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('layouts.frontend.g-play', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="background-close login-full">
        <div class="login-box">
            <?php echo $__env->make('frontend.auth.login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
    </div>
    <script src="<?php echo e(asset('frontend/js/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/js/popper.min.js')); ?>"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="<?php echo e(asset('frontend/js/app.js')); ?>"></script>
</body>

</html>
