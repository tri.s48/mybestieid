

<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Account Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Account Management</a>
            </li>
            <li class="active">
                <strong>View</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<?php echo $__env->make('layouts.backend.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?> 

<?php $__env->startSection('contentCrud'); ?>
<?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Account Management
            </div>
            <div class="panel-body">
               <div class="form-group">
                    <div class="col-sm-12">
                        <table class="table table-bordered" id="table-revenue-creator">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>User Master</th>
                                    <th>User Parent</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                        $master = App\Model\User::where(['id'=> $data->user_master])->first();
                                        $child = App\Model\User::where(['id'=> $data->user_child])->first();
                                    ?>
                                    <tr>
                                        <td><?php echo e($i); ?></td>
                                        <td><?php echo e($master->name); ?></td>
                                        <td><?php echo e($child->name); ?> <?php echo e($data->id); ?></td>
                                        <td align='center'><a href="<?php echo e(route('account_management.delete', [$data->user_master, $data->user_child])); ?>" class="btn btn-md btn-primary"><i class="fa fa-trash"></i></a></td>
                                    </tr>
                                <?php $i++; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>

    <?php $__env->startSection('scripts'); ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>

        <script src="<?php echo e(asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/plugins/bootstrap-select.min.js')); ?>"></script>
        <?php echo $__env->make('layouts.backend.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <script>    
            $(document).ready(function(){
                $('#table-revenue-creator').DataTable();
                $("div").removeClass("ui-toolbar");
            });
        </script>
    <?php $__env->stopSection(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>