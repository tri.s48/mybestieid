<?php $__env->startSection('css'); ?>
    <?php echo $__env->make('layouts.backend.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $dataTable->table(['width' => '100%']); ?>


<?php $__env->startSection('scripts'); ?>
    <?php echo $__env->make('layouts.backend.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $dataTable->scripts(); ?>

    <script>
        function changeRecommend(_id) {
            $.ajax({
                <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                    url: "<?php echo e(route('admin.channel.updateHD')); ?>",
                <?php elseif(auth()->user()->roles->first()->name === 'user'): ?>
                    url: "<?php echo e(route('user.channel.updateHD')); ?>",
                <?php else: ?>
                    url: "<?php echo e(route('creator.channel.updateHD')); ?>",
                <?php endif; ?>
                method: "POST",
                data: {
                    id: _id
                },
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
                success: function(response) {
                    console.log(response);
                    toastr.success("Mengubah HD Berhasil");
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }
        $(document).on('click', '.featured', function(e) {
            var id = $(this).attr('data-id');
            changeRecommend(id);
        });
    </script>
    <?php echo $__env->make('layouts.backend.datatables_limit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
