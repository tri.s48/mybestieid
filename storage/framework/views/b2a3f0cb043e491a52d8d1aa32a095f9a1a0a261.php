<?php $__env->startSection('css'); ?>
    <?php echo $__env->make('layouts.backend.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<table class="table" id="table">
    <thead>
        <tr>
            <?php if(auth()->user()->roles->first()->name == 'admin' ||
                auth()->user()->roles->first()->name == 'legal' ||
                auth()->user()->roles->first()->name == 'anr'): ?>
                <th class="text-center">Updated</th>
                <th class="text-center">Status</th>
            <?php endif; ?>
            <th class="text-center">Judul</th>
            <th class="text-center">Album</th>
            <th class="text-center">Tanggal Release</th>
            <th class="text-center">Penyanyi</th>
            <th class="text-center">Pencipta</th>
            <?php if(auth()->user()->roles->first()->name == 'admin' ||
                auth()->user()->roles->first()->name == 'legal' ||
                auth()->user()->roles->first()->name == 'anr'): ?>
                <th class="text-center">Action</th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $masterlagu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <?php if(auth()->user()->roles->first()->name == 'admin' ||
                    auth()->user()->roles->first()->name == 'legal' ||
                    auth()->user()->roles->first()->name == 'anr'): ?>
                    <td class="text-center" data-sort="<?php echo e(strtotime($m->updated_at)); ?>">
                        <?php
                            if ($m->updated_at == '0000-00-00 00:00:00') {
                                $dateupdated = '';
                            } else {
                                $dateupdated = date('d-m-Y h:m:s', strtotime($m->updated_at));
                            }
                            echo $dateupdated;
                        ?>
                    </td>
                    <td class="text-center" data-sort="<?php echo e(strtotime($m->status)); ?>">
                        <b>
                            <?php
                                if ($m->status == '1') {
                                    $sttus = 'Publish';
                                } else {
                                    $sttus = 'Draft';
                                }
                                echo $sttus;
                            ?>
                        </b>
                    </td>
                <?php endif; ?>
                <td>
                    <?php
                        if (
                            auth()
                                ->user()
                                ->roles->first()->name == 'admin'
                        ) {
                            $route = 'masterlagu.detail.lagu';
                        } elseif (
                            auth()
                                ->user()
                                ->roles->first()->name == 'legal'
                        ) {
                            $route = 'legal.masterlagu.detail.lagu';
                        } elseif (
                            auth()
                                ->user()
                                ->roles->first()->name == 'creator'
                        ) {
                            $route = 'creator.masterlagu.detail.lagu';
                        } elseif (
                            auth()
                                ->user()
                                ->roles->first()->name == 'singer'
                        ) {
                            $route = 'singer.masterlagu.detail.lagu';
                        } elseif (
                            auth()
                                ->user()
                                ->roles->first()->name == 'songwriter'
                        ) {
                            $route = 'songwriter.masterlagu.detail.lagu';
                        } elseif (
                            auth()
                                ->user()
                                ->roles->first()->name == 'anr'
                        ) {
                            $route = 'anr.masterlagu.detail.lagu';
                        }
                        echo '<a href="' . route($route, $m->id) . '">' . $m->track_title . '</a>';
                    ?>
                </td>
                <td><?php echo e($m->release_title); ?></td>
                <td class="text-center">
                    <?php
                        if ($m->release_date == '0000-00-00') {
                            $daterelease = '';
                        } else {
                            $daterelease = date('d-m-Y', strtotime($m->release_date));
                        }
                        echo $daterelease;
                    ?>
                </td>
                <td>
                    <?php
                        $sing = '';
                    ?>
                    <?php $__currentLoopData = $m->rolelagu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $singer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(!empty($singer->penyanyi)): ?>
                            <?php
                                if (
                                    auth()
                                        ->user()
                                        ->roles->first()->name == 'admin'
                                ) {
                                    $sing .= '<a href="' . route('masterlagu.detail.singer', $singer->penyanyi->id) . '">' . $singer->penyanyi->name_master . '</a> & ';
                                } elseif (
                                    auth()
                                        ->user()
                                        ->roles->first()->name == 'legal'
                                ) {
                                    $sing .= '<a href="' . route('legal.masterlagu.detail.singer', $singer->penyanyi->id) . '">' . $singer->penyanyi->name_master . '</a> & ';
                                } elseif (
                                    auth()
                                        ->user()
                                        ->roles->first()->name == 'anr'
                                ) {
                                    $sing .= '<a href="' . route('anr.masterlagu.detail.singer', $singer->penyanyi->id) . '">' . $singer->penyanyi->name_master . '</a> & ';
                                } else {
                                    $sing .= $singer->penyanyi->name_master . ' & ';
                                }
                            ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        echo rtrim($sing, ' & ');
                    ?>
                </td>
                <td>
                    <?php
                        $song = '';
                    ?>
                    <?php $__currentLoopData = $m->rolelagu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $songwriter): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(!empty($songwriter->pencipta)): ?>
                            <?php
                                if (
                                    auth()
                                        ->user()
                                        ->roles->first()->name == 'admin'
                                ) {
                                    $song .= '<a href="' . route('masterlagu.detail.songwriter', $songwriter->pencipta->id) . '">' . $songwriter->pencipta->name_master . '</a> & ';
                                } elseif (
                                    auth()
                                        ->user()
                                        ->roles->first()->name == 'legal'
                                ) {
                                    $song .= '<a href="' . route('legal.masterlagu.detail.songwriter', $songwriter->pencipta->id) . '">' . $songwriter->pencipta->name_master . '</a> & ';
                                } elseif (
                                    auth()
                                        ->user()
                                        ->roles->first()->name == 'anr'
                                ) {
                                    $song .= '<a href="' . route('anr.masterlagu.detail.songwriter', $songwriter->pencipta->id) . '">' . $songwriter->pencipta->name_master . '</a> & ';
                                } else {
                                    $song .= $songwriter->pencipta->name_master . ' & ';
                                }
                            ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        echo rtrim($song, ' & ');
                    ?>
                </td>
                <?php if(auth()->user()->roles->first()->name == 'admin' ||
                    auth()->user()->roles->first()->name == 'legal' ||
                    auth()->user()->roles->first()->name == 'anr'): ?>
                    <td class="text-center"><?php echo $__env->make('backend.masterlagu.datatables_actions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>

<?php $__env->startSection('scripts'); ?>
    <?php echo $__env->make('layouts.backend.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script>
        $(document).ready(function() {
            var dataTable = $('#table').DataTable({
                dom: 'Brtlip',
                order: [
                    [0, "desc"]
                ],
                processing: true,
                serverMethod: 'post',
                responsive: true,
                autoWidth: false,
                aLengthMenu: [
                    [10, 50, 100, 250, -1],
                    [10, 50, 100, 250, 'All']
                ],
                columnDefs: [{
                    "width": "10%",
                    "targets": 0
                }],
                buttons: [
                    <?php if(auth()->user()->roles->first()->name == 'admin' ||
                        auth()->user()->roles->first()->name == 'legal' ||
                        auth()->user()->roles->first()->name == 'anr'): ?>
                        {
                        "extend" : 'create',
                        "text" : '<i class="fa fa-plus"></i> Add New',
                        "className" : 'btn-primary',
                        "action" : function( e, dt, button, config){
                        window.location = "masterlagu/create";
                        }
                        },
                    <?php endif; ?> {
                        "extend": 'csv',
                        "text": '<i class="fa fa-file-excel-o"></i> CSV',
                        "className": 'btn-primary',
                        'action': function(e, dt, button, config) {
                            window.open("masterlagu/export_csv", "_blank");
                        }
                    },
                    {
                        "extend": 'excel',
                        "text": '<i class="fa fa-file-excel-o"></i> Excel',
                        "className": 'btn-primary',
                        'action': function(e, dt, button, config) {
                            window.open("masterlagu/export_excell", "_blank");
                        }
                    },
                    {
                        "extend": 'pdf',
                        "text": '<i class="fa fa-file-pdf-o"></i> PDF',
                        "className": 'btn-primary',
                        'action': function(e, dt, button, config) {
                            window.open("masterlagu/export_print", "_blank");
                        }
                    }
                ],
            });
            $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function() {
                $(".alert-danger").slideUp(1000);
            });
            $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function() {
                $(".alert-success").slideUp(1000);
            });
            //lengthmenu -> add a margin to the right and reset clear 
            $(".dataTables_length").css('clear', 'none');
            $(".dataTables_length").css('margin-right', '20px');

            //info -> reset clear and padding
            $(".dataTables_info").css('clear', 'none');
            $(".dataTables_info").css('padding', '0');
            $("div").removeClass("ui-toolbar");

            $('#singer').on('change', function() {
                $('#album').val('');
                dataTable.search(this.value).draw();
            });
            $('#album').on('change', function() {
                $('#singer').val('');
                dataTable.search(this.value).draw();
            });
            $('#status').on('change', function() {
                $('#singer').val('');
                $('#album').val('');
                dataTable.search(this.value).draw();
            });
            $('.form-inline').find('.clear-filtering').on('click', function(e) {
                $('#singer').val('');
                $('#album').val('');
                $('#status').val('');
                dataTable.search(this.value).draw();
                e.preventDefault();
            });
        });
    </script>
<?php $__env->stopSection(); ?>
