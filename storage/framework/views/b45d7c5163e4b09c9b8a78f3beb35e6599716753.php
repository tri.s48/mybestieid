

<?php $__env->startSection('breadcrumb'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Series</h2>
            <ol class="breadcrumb">
                <li>
                    <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                        <a href="<?php echo e(route('admin.serie.index')); ?>">Series</a>
                    <?php elseif(auth()->user()->roles->first()->name === 'user'): ?>
                        <a href="<?php echo e(route('user.serie.index')); ?>">Series</a>
                    <?php else: ?>
                        <a href="<?php echo e(route('creator.serie.index')); ?>">Series</a>
                    <?php endif; ?>
                </li>
                <li class="active">
                    <strong>Edit</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentCrud'); ?>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Series</h5>
        </div>
        <div class="ibox-content">
            <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                <?php echo Form::model($series, ['route' => ['admin.serie.update', $series->id], 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'forms', 'files' => true]); ?>

                <?php echo $__env->make('backend.series.fields', ['formType' => 'edit'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo Form::close(); ?>

            <?php elseif(auth()->user()->roles->first()->name === 'user'): ?>
                <?php echo Form::model($series, ['route' => ['user.serie.update', $series->id], 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'forms', 'files' => true]); ?>

                <?php echo $__env->make('backend.series.fields', ['formType' => 'edit'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo Form::close(); ?>

            <?php else: ?>
                <?php echo Form::model($series, ['route' => ['creator.serie.update', $series->id], 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'forms', 'files' => true]); ?>

                <?php echo $__env->make('backend.series.fields', ['formType' => 'edit'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo Form::close(); ?>

            <?php endif; ?>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>