<?php if(auth()->user()->roles->first()->name == 'admin'): ?>
    <div class='btn-group'>
        <a href="<?php echo e(route('masterlagu.edit', $m->id)); ?>" data-url="<?php echo e(route('masterlagu.edit', $m->id)); ?>"
            class='btn btn-primary btn-xs' title="Edit Master Lagu">
            <i class="fa fa-pencil"></i> Edit
        </a>
        <a href="<?php echo e(route('masterlagu.destroy', $m->id)); ?>" class='btn btn-danger btn-xs' title="Delete Master Lagu"
            onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
            <i class="fa fa-trash"></i> Delete
        </a>
    </div>
<?php elseif(auth()->user()->roles->first()->name == 'legal'): ?>
    <div class='btn-group'>
        <a href="<?php echo e(route('legal.masterlagu.edit', $m->id)); ?>"
            data-url="<?php echo e(route('legal.masterlagu.edit', $m->id)); ?>" class='btn btn-primary btn-xs'
            title="Edit Master Lagu">
            <i class="fa fa-pencil"></i> Edit
        </a>
        
    </div>
<?php elseif(auth()->user()->roles->first()->name == 'anr'): ?>
    <div class='btn-group'>
        <a href="<?php echo e(route('anr.masterlagu.edit', $m->id)); ?>" data-url="<?php echo e(route('anr.masterlagu.edit', $m->id)); ?>"
            class='btn btn-primary btn-xs' title="Edit Master Lagu">
            <i class="fa fa-pencil"></i> Edit
        </a>
        
    </div>
<?php endif; ?>
