<?php $__env->startSection('content'); ?>
<div class="row content-section">
    <div class="col-sm-12 col-md-12">
        <div id="myCarousel" class="carousel slide w-100" data-ride="carousel">
            <ol class="carousel-indicators float-right">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner w-100" role="listbox">
                <div class="carousel-item active">
                    <div class="col-xxl-5 col-lg-8 col-md-12">
                        <img height="370" width="750" class="img-fluid"
                            src="images/slideshow-home/Slide_1_Radja.jpg">
                        <a href="#" class="carousel-detail">
                            <div>
                                <!-- <h3>Breaking bad <span><i class="fa fa-star"></i> 6.1/10</span></h3>
      <p>Frustrated by the constant quarrel between the members of his dysfunctional family, Max
        loses interest to celebrate Christmas, awakening Krampus, a demon who wi...</p> -->
                                <button><i class="fa fa-play"></i> <span>Watch
                                        Now<span></button>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="col-xxl-5 col-lg-8 col-md-12">
                        <img height="370" width="750" class="img-fluid"
                            src="images/slideshow-home/Slide_2_ Inul.jpg">
                        <a href="/serie/3/just-me-and-you.html" class="carousel-detail">
                            <div>
                                <!-- <h3>The Final Season <span><i class="fa fa-star"></i> 8.3/10</span></h3>
      <p>The survivors of a plane crash find themselves stranded on a mysterious island. They are
        forced to work together for their survival when they realise that they...</p> -->
                                <button><i class="fa fa-play"></i> <span>Watch
                                        Now<span></button>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 ">
        <a href="/channels/all/all/views.html" class="title-more">
            <span>Featured LiveTV</span>
            <i class="fa fa-th float-right"></i>
        </a>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            <a class="channel " href="/channel/40/vir-h1.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/93ae4fbe1f6ac1f1d7bd9f0cb4233fc5.png" />
                <div>
                    <span>HD</span>
                    <p class="two">LIVE</p>
                </div>
            </a>
            <a class="channel " href="/channel/39/vir-hd2.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/ecac42921b3a406189d15646feb7f8e0.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/35/sky-news-arabia.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/28a0b4dae02f3575a03d4266d5a48dcb.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/34/vir-kids.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/0866865edff198c46ab7fc3b4aae49d8.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/33/vir-news.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/74b284bf5aec7cacc38688769b5ea88d.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/32/vir-movies.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/da82f2f4a7ae28281edf58ad0ccd97b1.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/31/vir-series.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/4d55a65d6a70d79e80a2530f9311664e.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/26/vir-comidy.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/dc5628c006fb0d2859b33c2adb750660.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/25/vir-drama.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/9818958dc455804c53caa91b4578212d.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/24/vir-music.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/9571df4be2a502b730bf61ecf1a01fc2.png" />
                <div>
                    <span>HD</span>
                    <p class="two">LIVE</p>
                </div>
            </a>
            <a class="channel " href="/channel/23/vir-doc.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/c3317ccf2a836d46f7adfe6d56d000d3.png" />
                <div>
                    <span>HD</span>
                    <p class="two">LIVE</p>
                </div>
            </a>
            <a class="channel " href="/channel/22/vir-culture.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/4986aef32e2f584f972bd9a71efe7594.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/21/vir-live.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/0000cdfe3b81f1c68a698b74194769c5.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/20/vir-history.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/9f096fd0a6374b14103abd5e7e070ac3.png" />
                <div>
                </div>
            </a>
            <a class="channel " href="/channel/19/vir-art.html">
                <img height="75" width="150"
                    src="https://webflix.virmana.com/uploads/cache/channel_thumb_web/uploads/png/24a78fa9f198bd009e73f0168c95fab1.png" />
                <div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 ">
        <a href="/actors.html" class="title-more">
            <span>Popular Actors</span>
            <i class="fa fa-th float-right"></i>
        </a>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            <a href="profile-actor.html" class="actor">
                <img src="images/stories/ActorDemian.jpg" />
                <span>Demian</span>
            </a>
            <a href="profile-actor.html" class="actor">
                <img src="images/stories/ActorErieSusan.jpg" />
                <span>Erie S</span>
            </a>
            <a href="profile-actor.html" class="actor">
                <img src="images/stories/ActorIanKasela.jpg" />
                <span>Ian K</span>
            </a>
            <a href="" class="actor">
                <img src="images/stories/ActorIisDahlia.jpg" />
                <span>Iis D</span>
            </a>
            <a href="profile-actor.html" class="actor">
                <img src="images/stories/Actor_Ine.jpg" />
                <span>Ine S</span>
            </a>
            <a href="profile-actor.html" class="actor">
                <img src="images/stories/ActorInulD.jpg" />
                <span>Inul D</span>
            </a>
            <a href="profile-actor.html" class="actor">
                <img src="images/stories/ActorFashaUngu.jpg" />
                <span>Pasha</span>
            </a>
            <a href="profile-actor.html" class="actor">
                <img src="images/stories/ActorOpik.jpg" />
                <span>Opik</span>
            </a>
            <a href="profile-actor.html" class="actor">
                <img src="images/stories/ActorSaefulJamil.jpg" />
                <span>Saeful J</span>
            </a>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 ">
        <a href="/filter/all/rating.html" class="title-more">
            <span>Top Rated</span>
            <i class="fa fa-th float-right"></i>
        </a>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            <a href="/serie/27/green-space.html" title="Green space" class="poster ">
                <div>
                    <img width="170" height="255" src="images/cover-home/Cover 4_Radja.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/5/to-paris-by-bicycle.html" alt="To paris by bicycle"
                class="poster ">
                <div>
                    <img width="170" height="255" src="images/cover-home/Cover1_Demian.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/13/the-real-christmas-gift.html" alt="The Real christmas gift"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/fe03f20b3c6df639317dbb41e7886ded.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/18/first-day.html" alt="First day" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/bddf8422e9fdb610f9d5a0b0874fb934.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/40/the-real-legend.html" title="The real legend"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/85cb9733e8424d7f956cdf51b99756c1.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/43/happy-new-life.html" title="Happy new life" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/51ef1caa9ff8f2b625d06b130f3e2e5d.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/8/one-love.html" title="One love" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/bad9a193da8908a17b8cb855842e3036.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/4/breaking-bad.html" title="Breaking Bad" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/0839d912a6fe9ac2ce8c27c713831bef.jpg" />
                    <div>
                        <span>EP16</span>
                        <p class="two">HD</p>
                    </div>
                </div>
            </a>
            <a href="/serie/7/snow-day.html" title="Snow Day" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/11ef385612bd06d0d8937e5b4fc8a7f8.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/36/elit-author.html" title="Elit author" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/305409501dbf0486954e50067bd9f192.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/2/my-last-chance.html" alt="MY LAST CHANCE" class="poster ">
                <div>
                    <img width="170" height="255" src="images/cover-home/Cover5_ErieSusan.jpg" />
                    <div>
                        <p>2010</p>
                    </div>
                </div>
            </a>
            <a href="/serie/12/bigg-party.html" title="Bigg party" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/568077f25ccdcea415c7cd830b5e7831.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/45/dark-year.html" title="Dark Year" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/a8e3406fa37e41efa5d4a03a9e561b27.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/30/fight-for-the-end.html" alt="Fight for the end"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/4116852b53c4c4f6589ce53a50b4d373.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/32/the-room-8.html" title="The room 8" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/af497057e13033bf01b2b4c3ec68be66.jpg" />
                    <div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 ">
        <a href="/filter/all/views.html" class="title-more">
            <span>Popular</span>
            <i class="fa fa-th float-right"></i>
        </a>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            <a href="/serie/4/breaking-bad.html" title="Breaking Bad" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/0839d912a6fe9ac2ce8c27c713831bef.jpg" />
                    <div>
                        <span>EP16</span>
                        <p class="two">HD</p>
                    </div>
                </div>
            </a>
            <a href="/movie/41/my-last-hope.html" alt="My last hope" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/ca5dd5c81f1a89cef415d7776eb6798c.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/3/just-me-and-you.html" title="Just me and you" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8af488ad2fc117d2621fbf4e3568860a.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/7/snow-day.html" title="Snow Day" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/11ef385612bd06d0d8937e5b4fc8a7f8.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/45/dark-year.html" title="Dark Year" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/a8e3406fa37e41efa5d4a03a9e561b27.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/2/my-last-chance.html" alt="MY LAST CHANCE" class="poster ">
                <div>
                    <img width="170" height="255" src="images/cover-home/Cover5_ErieSusan.jpg" />
                    <div>
                        <p>2010</p>
                    </div>
                </div>
            </a>
            <a href="/movie/5/to-paris-by-bicycle.html" alt="To paris by bicycle"
                class="poster ">
                <div>
                    <img width="170" height="255" src="images/cover-home/Cover 7_IneSinthua.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/20/just-do-it.html" title="Just do it" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/294a913c8f3d2bf0a667c667aa9a5e91.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/18/first-day.html" alt="First day" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/bddf8422e9fdb610f9d5a0b0874fb934.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/37/the-best-compnay.html" alt="The best compnay"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/51fc44e0a13f84308903dbcce3716c9d.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/33/zero-cross-love.html" alt="Zero Cross Love" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/4e958b4749382057fe330717eed88e8f.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/39/the-last-mistake-forever.html" alt="The last mistake forever"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/92436a77d14e5861ab2c4089b014e0c5.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/22/java-creative-developers.html" alt="Java Creative developers"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8f5c711c8825f5e153488602a7b307d7.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/9/africa-waves.html" alt="Africa waves" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8d126847144d44f77290ecf3db8bbaf8.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/10/red-fight-10.html" alt="Red Fight 10" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/015ac9d895a3a7bc00422d895a711d83.jpg" />
                    <div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 ">
        <a href="/filter/Action/newest.html" class="title-more">
            <span>Action</span>
            <i class="fa fa-th float-right"></i>
        </a>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            <a href="/serie/4/breaking-bad.html" title="Breaking Bad" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/0839d912a6fe9ac2ce8c27c713831bef.jpg" />
                    <div>
                        <span>EP16</span>
                        <p class="two">HD</p>
                    </div>
                </div>
            </a>
            <a href="/movie/37/the-best-compnay.html" alt="The best compnay"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/51fc44e0a13f84308903dbcce3716c9d.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/36/elit-author.html" title="Elit author" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/305409501dbf0486954e50067bd9f192.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/16/the-25th-century.html" title="the 25th century"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/bae15180135c71fbb6d2d9984e7f087a.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/15/musical-day-virmana-mistro.html" alt="Musical day / virmana mistro"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/9e75496d2ab75ed3a7ead16fd3bdea1f.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/14/the-best-winner.html" alt="The best winner" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/73ae18d3c98b19d6082dda70398207ee.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/2/my-last-chance.html" alt="MY LAST CHANCE" class="poster ">
                <div>
                    <img width="170" height="255" src="images/cover-home/Cover5_ErieSusan.jpg" />
                    <div>
                        <p>2010</p>
                    </div>
                </div>
            </a>
            <a href="/movie/1/inception.html" alt="Be your self" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/6ea2906db1748f2bea148b2e111bcedb.jpg" />
                    <div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 ">
        <a href="/filter/Adventure/newest.html" class="title-more">
            <span>Adventure</span>
            <i class="fa fa-th float-right"></i>
        </a>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            <a href="/serie/3/just-me-and-you.html" title="Just me and you" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8af488ad2fc117d2621fbf4e3568860a.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/20/just-do-it.html" title="Just do it" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/294a913c8f3d2bf0a667c667aa9a5e91.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/42/my-camera-eyes.html" title="My camera eyes" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/69d5d619448ef0dad34067a1ea0c26c3.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/29/black-birth-under-the-rain.html" alt="Black birth under the rain"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8c3c77eb0650b5456faac483bb3b8003.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/28/magico-manigico.html" alt="Magico Manigico" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/72f240547adcfdbd93bb0c6715319030.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/22/java-creative-developers.html" alt="Java Creative developers"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8f5c711c8825f5e153488602a7b307d7.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/19/after-the-festival.html" alt="After the festival"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/344eb33f3cfd0d5f6d3f161a76c373d7.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/15/musical-day-virmana-mistro.html" alt="Musical day / virmana mistro"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/9e75496d2ab75ed3a7ead16fd3bdea1f.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/14/the-best-winner.html" alt="The best winner" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/73ae18d3c98b19d6082dda70398207ee.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/12/bigg-party.html" title="Bigg party" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/568077f25ccdcea415c7cd830b5e7831.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/10/red-fight-10.html" alt="Red Fight 10" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/015ac9d895a3a7bc00422d895a711d83.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/5/to-paris-by-bicycle.html" alt="To paris by bicycle"
                class="poster ">
                <div>
                    <img width="170" height="255" src="images/cover-home/Cover5_ErieSusan.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/2/my-last-chance.html" alt="MY LAST CHANCE" class="poster ">
                <div>
                    <img width="170" height="255" src="images/cover-home/Cover5_ErieSusan.jpg" />
                    <div>
                        <p>2010</p>
                    </div>
                </div>
            </a>
            <a href="/movie/1/inception.html" alt="Be your self" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/6ea2906db1748f2bea148b2e111bcedb.jpg" />
                    <div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 ">
        <a href="/filter/Crime/newest.html" class="title-more">
            <span>Crime</span>
            <i class="fa fa-th float-right"></i>
        </a>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            <a href="/serie/4/breaking-bad.html" title="Breaking Bad" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/0839d912a6fe9ac2ce8c27c713831bef.jpg" />
                    <div>
                        <span>EP16</span>
                        <p class="two">HD</p>
                    </div>
                </div>
            </a>
            <a href="/serie/42/my-camera-eyes.html" title="My camera eyes" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/69d5d619448ef0dad34067a1ea0c26c3.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/33/zero-cross-love.html" alt="Zero Cross Love" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/4e958b4749382057fe330717eed88e8f.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/32/the-room-8.html" title="The room 8" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/af497057e13033bf01b2b4c3ec68be66.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/31/virmana-enet-musical-day.html" alt="Virmana Enet Musical day"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/41334be9ec20285805ed42706045646f.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/27/green-space.html" title="Green space" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/c4ec1190076d35be193ce6cc68d9ccc5.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/26/the-last-women-in-the-earth.html" title="The last women in the earth"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/5259e2beb85f2da4be4e948b9ba77555.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/16/the-25th-century.html" title="the 25th century"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/bae15180135c71fbb6d2d9984e7f087a.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/8/one-love.html" title="One love" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/bad9a193da8908a17b8cb855842e3036.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/6/swimming-there.html" alt="Swimming there" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/d70f7232562b86bebb29755c121b563e.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/5/to-paris-by-bicycle.html" alt="To paris by bicycle"
                class="poster ">
                <div>
                    <img width="170" height="255" src="images/cover-home/Cover5_ErieSusan.jpg" />
                    <div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 ">
        <a href="/filter/Fantasy/newest.html" class="title-more">
            <span>Fantasy</span>
            <i class="fa fa-th float-right"></i>
        </a>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            <a href="/serie/3/just-me-and-you.html" title="Just me and you" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8af488ad2fc117d2621fbf4e3568860a.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/42/my-camera-eyes.html" title="My camera eyes" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/69d5d619448ef0dad34067a1ea0c26c3.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/41/my-last-hope.html" alt="My last hope" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/ca5dd5c81f1a89cef415d7776eb6798c.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/35/waiting-my-familly.html" alt="Waiting my Familly"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/1bdaec81039782dc38e29dce9f56baad.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/30/fight-for-the-end.html" alt="Fight for the end"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/4116852b53c4c4f6589ce53a50b4d373.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/26/the-last-women-in-the-earth.html" title="The last women in the earth"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/5259e2beb85f2da4be4e948b9ba77555.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/21/my-soul.html" alt="My soul" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/66160c9a222f36f35b2ad0f93bc9d367.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/19/after-the-festival.html" alt="After the festival"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/344eb33f3cfd0d5f6d3f161a76c373d7.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/17/behind-the-black-wall.html" title="Behind the black wall"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/7f7ed862c79e0b3753fe2b2638a3c730.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/12/bigg-party.html" title="Bigg party" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/568077f25ccdcea415c7cd830b5e7831.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/9/africa-waves.html" alt="Africa waves" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8d126847144d44f77290ecf3db8bbaf8.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/6/swimming-there.html" alt="Swimming there" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/d70f7232562b86bebb29755c121b563e.jpg" />
                    <div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 ">
        <a href="/filter/Family/newest.html" class="title-more">
            <span>Family</span>
            <i class="fa fa-th float-right"></i>
        </a>
    </div>
    <div class="flix-carousel">
        <button class="prev_btn"><i class="fa fa-chevron-left"></i></button>
        <button class="next_btn"><i class="fa fa-chevron-right"></i></button>
        <div class="flix-scroll-x">
            <a href="/serie/3/just-me-and-you.html" title="Just me and you" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8af488ad2fc117d2621fbf4e3568860a.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/20/just-do-it.html" title="Just do it" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/294a913c8f3d2bf0a667c667aa9a5e91.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/17/behind-the-black-wall.html" title="Behind the black wall"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/7f7ed862c79e0b3753fe2b2638a3c730.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/13/the-real-christmas-gift.html" alt="The Real christmas gift"
                class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/fe03f20b3c6df639317dbb41e7886ded.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/serie/11/master-chef.html" title="Master chef" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/93bcafd533688a07b835cdc0ac0c0b68.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/10/red-fight-10.html" alt="Red Fight 10" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/015ac9d895a3a7bc00422d895a711d83.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/9/africa-waves.html" alt="Africa waves" class="poster ">
                <div>
                    <img width="170" height="255"
                        src="https://webflix.virmana.com/uploads/cache/poster_thumb_web/uploads/jpg/8d126847144d44f77290ecf3db8bbaf8.jpg" />
                    <div>
                    </div>
                </div>
            </a>
            <a href="/movie/5/to-paris-by-bicycle.html" alt="To paris by bicycle"
                class="poster ">
                <div>
                    <img width="170" height="255" src="images/cover-home/Cover5_ErieSusan.jpg" />
                    <div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>