<?php $__env->startSection('css'); ?>
    <?php echo $__env->make('layouts.backend.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $dataTable->table(['width' => '100%']); ?>


<?php $__env->startSection('scripts'); ?>
    <?php echo $__env->make('layouts.backend.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $dataTable->scripts(); ?>

    <script>
        function changeRecommend(_id) {
            $.ajax({
                <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                    url: "<?php echo e(route('admin.posts.updateFeatured')); ?>",
                <?php elseif(auth()->user()->roles->first()->name === 'user'): ?>
                    url: "<?php echo e(route('user.posts.updateFeatured')); ?>",
                <?php else: ?>
                    url: "<?php echo e(route('creator.posts.updateFeatured')); ?>",
                <?php endif; ?>
                method: "POST",
                data: {
                    id: _id
                },
                headers: {
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
                success: function(response) {
                    console.log(response);
                    toastr.success("Mengubah Feature Berhasil");
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }
        $(document).on('click', '.featured', function(e) {
            var id = $(this).attr('data-id');
            changeRecommend(id);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
    </script>
    <?php echo $__env->make('layouts.backend.datatables_limit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
