<?php $__env->startSection('content'); ?>
<div class="row content-section">
    <div class="col-sm-12 col-md-12 ">
        <?php echo $data['content']; ?>

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>