<?php $__env->startSection('css'); ?>
    <?php echo $__env->make('layouts.backend.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">Name</th>
            <th class="text-center">Email</th>
            <?php if(auth()->user()->roles->first()->name == 'admin' ||
                auth()->user()->roles->first()->name == 'legal' ||
                auth()->user()->roles->first()->name == 'anr'): ?>
                <th class="text-center">Action</th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $creator; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td data-sort="<?php echo e(strtotime($c->updated_at)); ?>">
                    <b>
                        <?php echo e($c->name_master); ?>

                    </b>
                </td>
                <td><?php echo e($c->email); ?></td>
                <?php if(auth()->user()->roles->first()->name == 'admin' ||
                    auth()->user()->roles->first()->name == 'legal' ||
                    auth()->user()->roles->first()->name == 'anr'): ?>
                    <td class="text-center"><?php echo $__env->make('backend.creator.datatables_actions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>

<?php $__env->startSection('scripts'); ?>
    <?php echo $__env->make('layouts.backend.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script>
        $(document).ready(function() {
            var dataTable = $('#table').DataTable({
                <?php if(auth()->user()->roles->first()->name == 'admin' ||
                    auth()->user()->roles->first()->name == 'legal' ||
                    auth()->user()->roles->first()->name == 'anr'): ?>
                    dom : 'Brtlip',
                <?php endif; ?>
                order: [
                    [0, "desc"]
                ],
                processing: true,
                serverMethod: 'post',
                responsive: true,
                autoWidth: false,
                aLengthMenu: [
                    [10, 50, 100, 250, -1],
                    [10, 50, 100, 250, 'All']
                ],
                buttons: [
                    <?php if(auth()->user()->roles->first()->name == 'admin' ||
                        auth()->user()->roles->first()->name == 'legal' ||
                        auth()->user()->roles->first()->name == 'anr'): ?>
                        {
                        "extend" : 'create',
                        "text" : '<i class="fa fa-plus"></i> Add New',
                        "className" : 'btn-primary',
                        "action" : function( e, dt, button, config){
                        window.location = "mastersinger/create";
                        }
                        }
                    <?php endif; ?>
                ],
            });
            $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function() {
                $(".alert-danger").slideUp(1000);
            });
            $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function() {
                $(".alert-success").slideUp(1000);
            });
            //lengthmenu -> add a margin to the right and reset clear 
            $(".dataTables_length").css('clear', 'none');
            $(".dataTables_length").css('margin-right', '20px');

            //info -> reset clear and padding
            $(".dataTables_info").css('clear', 'none');
            $(".dataTables_info").css('padding', '0');
            $("div").removeClass("ui-toolbar");

            $('#singer').on('change', function() {
                $('#album').val('');
                dataTable.search(this.value).draw();
            });
            $('#album').on('change', function() {
                $('#singer').val('');
                dataTable.search(this.value).draw();
            });
            $('#status').on('change', function() {
                $('#singer').val('');
                $('#album').val('');
                dataTable.search(this.value).draw();
            });
            $('.form-inline').find('.clear-filtering').on('click', function(e) {
                $('#singer').val('');
                $('#album').val('');
                $('#status').val('');
                dataTable.search(this.value).draw();
                e.preventDefault();
            });
        });
    </script>
<?php $__env->stopSection(); ?>
