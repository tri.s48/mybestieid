<h2 class="page-header logo-app">
    <img src="images/logo.png">
</h2>
<div class="nav-bar-container">
    <nav class="navbar bg-light left-navbar">
        <ul class="navbar-nav">
            <a href="#" class="btn  btn-subscription">
                <i class="fas fa-star"></i> 
                Subscribe now !
            </a>
            
            <?php if(!empty(menu())): ?>    
                <?php $__currentLoopData = menu(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>    
                <li class="nav-item  <?php echo e(Request::segment(1) == $menu['url'] ? 'active' : ''); ?> ">
                    <a class="nav-link" href="<?php echo e($menu['url']); ?>"> 
                        <i class="<?php echo e($menu['icon']); ?>"></i> 
                        <?php echo e($menu['title']); ?>

                    </a>
                </li class="nav-item">
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>

            <li class="nav-item ">
                <a class="nav-link open-login " href="/mylist.html">
                    <i class="fas fa-folder"></i>
                    My list
                </a>
            </li>
        </ul>
    </nav>
</div>
<nav class="navbar bg-light left-navbar bottom-navbar">
    <a id="lightmode" href="#"><i class="fas fa-sun"></i></a>
    <a id="darkmode" href="#" style="display:none"><i class="fas fa-moon"></i> </a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="/contact-us.html">
                <i class="fas fa-envelope"></i>
                Contact us
            </a>
        </li class="nav-item">
        
        <?php if(!empty(page())): ?>  
            <?php $__currentLoopData = page(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e($page['slug']); ?>">
                    <i class="<?php echo e($page['attr_3']); ?>"></i> 
                    <?php echo e($page['title']); ?>

                </a>
            </li class="nav-item">
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

    </ul>
</nav>
