<?php if(auth()->user()->roles->first()->name === 'admin'): ?>
	<?php echo Form::open(['route' => ['admin.posts.destroy', $id], 'method' => 'delete']); ?>

	<?php if($post_type == 'manual'): ?>
	<div class='btn-group'>
		
	    <a href="<?php echo e(route('admin.posts.editManual', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
		</a>
		<?php if($is_active == 1): ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-warning btn-xs',
			'title' => 'Inactive Series',
			'onclick' => "return confirm('Do you want to inactive this series?')",
			'name' => 'action',
			'value' => 'inact'
		]); ?>

		<?php else: ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-ok"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-success btn-xs',
			'title' => 'Activated Series',
			'onclick' => "return confirm('Do you want to activated this series?')",
			'name' => 'action',
			'value' => 'act'
		]); ?>

		<?php endif; ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-remove"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-danger btn-xs',
			'title' => 'Delete Post',
			'onclick' => "return confirm('Do you want to delete this posts?')",
			'name' => 'action',
			'value' => 'del'
		]); ?>

	</div>
	<?php else: ?>
	<div class='btn-group'>
	    <a href="<?php echo e(route('admin.posts.editAutomatic', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
		</a>
		<?php if($is_active == 1): ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-warning btn-xs',
			'title' => 'Inactive Series',
			'onclick' => "return confirm('Do you want to inactive this series?')",
			'name' => 'action',
			'value' => 'inact'
		]); ?>

		<?php else: ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-ok"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-success btn-xs',
			'title' => 'Activated Series',
			'onclick' => "return confirm('Do you want to activated this series?')",
			'name' => 'action',
			'value' => 'act'
		]); ?>

		<?php endif; ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-remove"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-danger btn-xs',
			'title' => 'Delete Post',
			'onclick' => "return confirm('Do you want to delete this posts?')",
			'name' => 'action',
			'value' => 'del'
		]); ?>

	</div>
	<?php endif; ?>
	<?php echo Form::close(); ?>

<?php elseif(auth()->user()->roles->first()->name === 'user'): ?>
	<?php echo Form::open(['route' => ['user.posts.destroy', $id], 'method' => 'delete']); ?>

	<?php if($post_type == 'manual'): ?>
	<div class='btn-group'>
		<a href="<?php echo e(route('user.posts.editManual', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Post">
			<i class="glyphicon glyphicon-edit"></i>
		</a>
		<?php if($is_active == 1): ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-warning btn-xs',
			'title' => 'Inactive Series',
			'onclick' => "return confirm('Do you want to inactive this series?')",
			'name' => 'action',
			'value' => 'inact'
		]); ?>

		<?php else: ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-ok"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-success btn-xs',
			'title' => 'Activated Series',
			'onclick' => "return confirm('Do you want to activated this series?')",
			'name' => 'action',
			'value' => 'act'
		]); ?>

		<?php endif; ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-remove"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-danger btn-xs',
			'title' => 'Delete Post',
			'onclick' => "return confirm('Do you want to delete this posts?')",
			'name' => 'action',
			'value' => 'del'
		]); ?>

	</div>
	<?php else: ?>
	<div class='btn-group'>
	    <a href="<?php echo e(route('user.posts.editAutomatic', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
		</a>
		<?php if($is_active == 1): ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-warning btn-xs',
			'title' => 'Inactive Series',
			'onclick' => "return confirm('Do you want to inactive this series?')",
			'name' => 'action',
			'value' => 'inact'
		]); ?>

		<?php else: ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-ok"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-success btn-xs',
			'title' => 'Activated Series',
			'onclick' => "return confirm('Do you want to activated this series?')",
			'name' => 'action',
			'value' => 'act'
		]); ?>

		<?php endif; ?>
		<?php echo Form::button('<i class="glyphicon glyphicon-remove"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-danger btn-xs',
			'title' => 'Delete Post',
			'onclick' => "return confirm('Do you want to delete this posts?')",
			'name' => 'action',
			'value' => 'del'
		]); ?>

	</div>
	<?php endif; ?>
	<?php echo Form::close(); ?>

<?php else: ?>
	<?php echo Form::open(['route' => ['creator.posts.destroy', $id], 'method' => 'delete']); ?>

	<?php if($post_type == 'manual'): ?>
	<div class='btn-group'>
	    <a href="<?php echo e(route('creator.posts.editManual', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
	    </a>
	</div>
	<div class='btn-group'>
	    <?php if($is_active == 1): ?>
        <?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-warning btn-xs',
            'title' => 'Inactive Posts',
            'onclick' => "return confirm('Do you want to inactive this posts?')",
            'name' => 'action',
            'value' => 'inact'
        ]); ?>

        <?php endif; ?>
	</div>
	<?php else: ?>
	<div class='btn-group'>
	    <a href="<?php echo e(route('creator.posts.editAutomatic', $id)); ?>" class='btn btn-primary btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
	    </a>
	</div>
	<div class='btn-group'>
	    <?php if($is_active == 1): ?>
        <?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Inactive Posts',
            'onclick' => "return confirm('Do you want to inactive this posts?')",
            'name' => 'action',
            'value' => 'inact'
        ]); ?>

        <?php endif; ?>
	</div>
	<?php endif; ?>
	<?php echo Form::close(); ?>

<?php endif; ?>